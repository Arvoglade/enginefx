package ru.arvoglade.Engine.engineCore.scene;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.geometry.Point3D;
import javafx.scene.paint.Paint;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.arvoglade.Engine.engineCore.chunk.ChunkDataStructure;

import ru.arvoglade.Engine.model.Object3D;
import ru.arvoglade.Engine.model.Orientation;
import ru.arvoglade.Engine.model.Polygon3D;
import ru.arvoglade.Engine.process.Camera;

import java.util.*;

@Component("many-spheres")
@Scope("prototype")
public class ManyTrueSpheresEngineScene extends BeanEngineScene {

    private Random random = new Random();

    @Override
    public String getName() {
        return "many-spheres";
    }

    @Override
    public Camera getCameraSettingsDefault() {
        return cameraSettings;
    }

    @Override
    public Camera getCameraSettingsForUser(String id) {
        return cameraSettings;
    }

    @Override
    public ChunkDataStructure getChunkDataStructure() {
        return chunkDataStructure;
    }

    @Override
    public void loadForChunk(String chunkId) {

    }

    @Override
    public void loadForUser(String userId) {

    }

    @Override
    public void unloadForChunk(String chunkId) {

    }

    @Override
    public void unloadExceptChunk(String chunkId) {

    }

    public ManyTrueSpheresEngineScene() {

        cameraSettings = new Camera();
        chunkDataStructure = new ChunkDataStructure(1, 500);
        //cameraSettings.setLocation(new Point3D(-3850, -3837, -3838));
        //cameraSettings.setLocation(new Point3D(1463, -4702, 1377));
        cameraSettings.setOrientation(new Orientation(44 * Math.PI / 180, 35 * Math.PI / 180, 0));

        int n, m, k;
        n = 5;
        m = 5;
        k = 5;
        double offset = 400;
        Map<String, Object3D> map = new HashMap<>();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                for (int t = 0; t < k; t++) {
                    Object3D sphere = getTrueSphere(1, 0);
                    sphere.setLocation(new Point3D(i * offset, j * offset, t * offset));
                    chunkDataStructure.addObject(sphere);
                }
            }
        }

        // TEST
        //sphereEngineScene.chunkDataStructure.getAllObjects().findFirst().get().setSpeedVector(new Point3D(8, 0, 0));

    }

    public Object3D getTrueSphere(double sizeMultiplier, int detailFactor) {
        Object3D sphere = new Object3D();

        sphere.setPolygons(getIcosahedronPolygons());
        sphere.getPolygons().forEach(polygon3D -> {
            List<Point3D> pointsNew = new ArrayList<>();
            polygon3D.getPoints().forEach(p -> pointsNew.add(p.multiply(sizeMultiplier)));
            polygon3D.setPoints(pointsNew);
        });

        for (int i = 0; i < detailFactor; i++) {
            sphere.setPolygons(detalizePolygon(sphere.getPolygons()));
        }

        Paint paint = randomPaint();
        sphere.getPolygons().forEach(p -> p.setPaint(paint));

        return sphere;
    }

    private Paint randomPaint() {
        int red = random.nextInt(200, 256);
        int blue = random.nextInt(220, 256);
        int green = random.nextInt(Math.min(red, blue), 256);
        String hexString = getoneByteHexString(red) + getoneByteHexString(green) + getoneByteHexString(blue);
        Paint paint = Paint.valueOf(hexString);
        return paint;
    }

    private String getoneByteHexString(int val) {
        String s = Integer.toHexString(val);
        if (s.length() != 2) {
            s = "0" + s;
        }
        return s;
    }

    private List<Polygon3D> detalizePolygon(List<Polygon3D> polygons) {
        List<Polygon3D> polygonsNew = new ArrayList<>(polygons.size() * 4);
        double d = polygons.get(0).getPoints().get(0).distance(Point3D.ZERO);
        polygons.forEach(polygon -> {

            Point3D p1, p2, p3, p12, p23, p31;
            p1 = polygon.getPoints().get(0);
            p2 = polygon.getPoints().get(1);
            p3 = polygon.getPoints().get(2);
            p12 = p1.interpolate(p2, 0.5);
            p23 = p2.interpolate(p3, 0.5);
            p31 = p3.interpolate(p1, 0.5);

            double d2 = p12.distance(Point3D.ZERO);

            double c = d / d2;

            p12 = p12.multiply(c);
            p23 = p23.multiply(c);
            p31 = p31.multiply(c);

            Polygon3D polygon1 = new Polygon3D(List.of(p1, p12, p31));
            Polygon3D polygon2 = new Polygon3D(List.of(p12, p2, p23));
            Polygon3D polygon3 = new Polygon3D(List.of(p23, p3, p31));
            Polygon3D polygon4 = new Polygon3D(List.of(p12, p23, p31));

            polygonsNew.add(polygon1);
            polygonsNew.add(polygon2);
            polygonsNew.add(polygon3);
            polygonsNew.add(polygon4);
        });

        return polygonsNew;
    }

    private List<Polygon3D> getIcosahedronPolygons() {

        List<List<TrueSphereEngineSceneConfig.Point3DMy>> points;

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            TypeReference<List<List<TrueSphereEngineSceneConfig.Point3DMy>>> ref1 = new TypeReference<>() {};
            points = objectMapper.readValue(getIcosahedronPolygonsData(), ref1);
            //System.out.println(objects.size());

        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        List<Polygon3D> polygons = new ArrayList<>();
        points.forEach(p -> {
            List<Point3D> onePolygonPoints = new ArrayList<>();
            p.forEach(pInner -> {
                onePolygonPoints.add(new Point3D(pInner.x, pInner.y, pInner.z));
            });
            polygons.add(new Polygon3D(onePolygonPoints));
        });
        return polygons;
    }

    private String getIcosahedronPolygonsData() {
        return """
                [
                 	[
                 		{
                 			"x": 0.0,
                 			"y": -80.90169943749474,
                 			"z": 50.0
                 		},
                 		{
                 			"x": 50.0,
                 			"y": 0.0,
                 			"z": 80.90169943749474
                 		},
                 		{
                 			"x": -50.0,
                 			"y": 0.0,
                 			"z": 80.90169943749474
                 		}
                 	],
                 	[
                 		{
                 			"x": -80.90169943749474,
                 			"y": -50.0,
                 			"z": 0.0
                 		},
                 		{
                 			"x": 0.0,
                 			"y": -80.90169943749474,
                 			"z": 50.0
                 		},
                 		{
                 			"x": -50.0,
                 			"y": 0.0,
                 			"z": 80.90169943749474
                 		}
                 	],
                 	[
                 		{
                 			"x": 80.90169943749474,
                 			"y": 50.0,
                 			"z": 0.0
                 		},
                 		{
                 			"x": 0.0,
                 			"y": 80.90169943749474,
                 			"z": -50.0
                 		},
                 		{
                 			"x": 0.0,
                 			"y": 80.90169943749474,
                 			"z": 50.0
                 		}
                 	],
                 	[
                 		{
                 			"x": -80.90169943749474,
                 			"y": -50.0,
                 			"z": 0.0
                 		},
                 		{
                 			"x": -50.0,
                 			"y": 0.0,
                 			"z": 80.90169943749474
                 		},
                 		{
                 			"x": -80.90169943749474,
                 			"y": 50.0,
                 			"z": 0.0
                 		}
                 	],
                 	[
                 		{
                 			"x": 80.90169943749474,
                 			"y": -50.0,
                 			"z": 0.0
                 		},
                 		{
                 			"x": 50.0,
                 			"y": 0.0,
                 			"z": 80.90169943749474
                 		},
                 		{
                 			"x": 0.0,
                 			"y": -80.90169943749474,
                 			"z": 50.0
                 		}
                 	],
                 	[
                 		{
                 			"x": -80.90169943749474,
                 			"y": 50.0,
                 			"z": 0.0
                 		},
                 		{
                 			"x": 0.0,
                 			"y": 80.90169943749474,
                 			"z": 50.0
                 		},
                 		{
                 			"x": 0.0,
                 			"y": 80.90169943749474,
                 			"z": -50.0
                 		}
                 	],
                 	[
                 		{
                 			"x": 0.0,
                 			"y": 80.90169943749474,
                 			"z": 50.0
                 		},
                 		{
                 			"x": -50.0,
                 			"y": 0.0,
                 			"z": 80.90169943749474
                 		},
                 		{
                 			"x": 50.0,
                 			"y": 0.0,
                 			"z": 80.90169943749474
                 		}
                 	],
                 	[
                 		{
                 			"x": 0.0,
                 			"y": -80.90169943749474,
                 			"z": -50.0
                 		},
                 		{
                 			"x": -50.0,
                 			"y": 0.0,
                 			"z": -80.90169943749474
                 		},
                 		{
                 			"x": 50.0,
                 			"y": 0.0,
                 			"z": -80.90169943749474
                 		}
                 	],
                 	[
                 		{
                 			"x": 0.0,
                 			"y": 80.90169943749474,
                 			"z": -50.0
                 		},
                 		{
                 			"x": 50.0,
                 			"y": 0.0,
                 			"z": -80.90169943749474
                 		},
                 		{
                 			"x": -50.0,
                 			"y": 0.0,
                 			"z": -80.90169943749474
                 		}
                 	],
                 	[
                 		{
                 			"x": 80.90169943749474,
                 			"y": 50.0,
                 			"z": 0.0
                 		},
                 		{
                 			"x": 50.0,
                 			"y": 0.0,
                 			"z": -80.90169943749474
                 		},
                 		{
                 			"x": 0.0,
                 			"y": 80.90169943749474,
                 			"z": -50.0
                 		}
                 	],
                 	[
                 		{
                 			"x": -80.90169943749474,
                 			"y": -50.0,
                 			"z": 0.0
                 		},
                 		{
                 			"x": -50.0,
                 			"y": 0.0,
                 			"z": -80.90169943749474
                 		},
                 		{
                 			"x": 0.0,
                 			"y": -80.90169943749474,
                 			"z": -50.0
                 		}
                 	],
                 	[
                 		{
                 			"x": -80.90169943749474,
                 			"y": -50.0,
                 			"z": 0.0
                 		},
                 		{
                 			"x": -80.90169943749474,
                 			"y": 50.0,
                 			"z": 0.0
                 		},
                 		{
                 			"x": -50.0,
                 			"y": 0.0,
                 			"z": -80.90169943749474
                 		}
                 	],
                 	[
                 		{
                 			"x": 80.90169943749474,
                 			"y": 50.0,
                 			"z": 0.0
                 		},
                 		{
                 			"x": 50.0,
                 			"y": 0.0,
                 			"z": 80.90169943749474
                 		},
                 		{
                 			"x": 80.90169943749474,
                 			"y": -50.0,
                 			"z": 0.0
                 		}
                 	],
                 	[
                 		{
                 			"x": 80.90169943749474,
                 			"y": 50.0,
                 			"z": 0.0
                 		},
                 		{
                 			"x": 0.0,
                 			"y": 80.90169943749474,
                 			"z": 50.0
                 		},
                 		{
                 			"x": 50.0,
                 			"y": 0.0,
                 			"z": 80.90169943749474
                 		}
                 	],
                 	[
                 		{
                 			"x": 80.90169943749474,
                 			"y": -50.0,
                 			"z": 0.0
                 		},
                 		{
                 			"x": 0.0,
                 			"y": -80.90169943749474,
                 			"z": -50.0
                 		},
                 		{
                 			"x": 50.0,
                 			"y": 0.0,
                 			"z": -80.90169943749474
                 		}
                 	],
                 	[
                 		{
                 			"x": -80.90169943749474,
                 			"y": -50.0,
                 			"z": 0.0
                 		},
                 		{
                 			"x": 0.0,
                 			"y": -80.90169943749474,
                 			"z": -50.0
                 		},
                 		{
                 			"x": 0.0,
                 			"y": -80.90169943749474,
                 			"z": 50.0
                 		}
                 	],
                 	[
                 		{
                 			"x": -80.90169943749474,
                 			"y": 50.0,
                 			"z": 0.0
                 		},
                 		{
                 			"x": -50.0,
                 			"y": 0.0,
                 			"z": 80.90169943749474
                 		},
                 		{
                 			"x": 0.0,
                 			"y": 80.90169943749474,
                 			"z": 50.0
                 		}
                 	],
                 	[
                 		{
                 			"x": -80.90169943749474,
                 			"y": 50.0,
                 			"z": 0.0
                 		},
                 		{
                 			"x": 0.0,
                 			"y": 80.90169943749474,
                 			"z": -50.0
                 		},
                 		{
                 			"x": -50.0,
                 			"y": 0.0,
                 			"z": -80.90169943749474
                 		}
                 	],
                 	[
                 		{
                 			"x": 80.90169943749474,
                 			"y": 50.0,
                 			"z": 0.0
                 		},
                 		{
                 			"x": 80.90169943749474,
                 			"y": -50.0,
                 			"z": 0.0
                 		},
                 		{
                 			"x": 50.0,
                 			"y": 0.0,
                 			"z": -80.90169943749474
                 		}
                 	],
                 	[
                 		{
                 			"x": 80.90169943749474,
                 			"y": -50.0,
                 			"z": 0.0
                 		},
                 		{
                 			"x": 0.0,
                 			"y": -80.90169943749474,
                 			"z": 50.0
                 		},
                 		{
                 			"x": 0.0,
                 			"y": -80.90169943749474,
                 			"z": -50.0
                 		}
                 	]
                 ]""";
    }


}
