package ru.arvoglade.Engine.engineCore.scene;

import ru.arvoglade.Engine.engineCore.chunk.ChunkDataStructure;
import ru.arvoglade.Engine.process.Camera;

public interface EngineScene {

    String getName();

    Camera getCameraSettingsDefault();

    Camera getCameraSettingsForUser(String id);

    ChunkDataStructure getChunkDataStructure();

    void loadForChunk(String chunkId);

    void loadForUser(String userId);

    void unloadForChunk(String chunkId);

    void unloadExceptChunk(String chunkId);

}
