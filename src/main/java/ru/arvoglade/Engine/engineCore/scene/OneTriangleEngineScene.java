package ru.arvoglade.Engine.engineCore.scene;

import javafx.geometry.Point3D;
import javafx.scene.paint.Paint;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.arvoglade.Engine.model.Object3D;
import ru.arvoglade.Engine.model.Polygon3D;

import java.util.ArrayList;
import java.util.List;

@Component("triangle")
@Scope("prototype")
public class OneTriangleEngineScene extends BeanEngineScene {

    public OneTriangleEngineScene() {

        Object3D object3D = new Object3D();
        object3D.setLocation(new Point3D(300, 0, 0));

        List<Polygon3D> polygons = new ArrayList<>();
        Polygon3D polygon1 = new Polygon3D(List.of(
                new Point3D(0, 150, 150),
                new Point3D(0, 150, -150),
                new Point3D(0, -150, 150)
        ));
        polygons.add(polygon1);
        
        polygons.forEach(p -> p.setPaint(Paint.valueOf("502030")));
        object3D.setPolygons(polygons);

        chunkDataStructure.addObject(object3D);
    }


    @Override
    public String getName() {
        return "triangle";
    }
}
