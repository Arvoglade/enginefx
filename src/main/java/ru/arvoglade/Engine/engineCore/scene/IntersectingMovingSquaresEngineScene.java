package ru.arvoglade.Engine.engineCore.scene;

import javafx.geometry.Point3D;
import javafx.scene.paint.Paint;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.arvoglade.Engine.model.Object3D;
import ru.arvoglade.Engine.model.Orientation;
import ru.arvoglade.Engine.model.Polygon3D;

import java.util.ArrayList;
import java.util.List;

@Component("intersecting-moving-squares")
@Scope("prototype")
public class IntersectingMovingSquaresEngineScene extends BeanEngineScene {

    public IntersectingMovingSquaresEngineScene() {

        Object3D square1 = getSquare();
        Object3D square2 = getSquare();

        square1.setLocation(new Point3D(500, 150, 0));
        square2.setLocation(new Point3D(500, -150, 0));

        double d90 = Math.PI / 2;
        double d30 = Math.PI / 180 * 30;
        square1.setOrientation(new Orientation(d90, d30, 0));
        square2.setOrientation(new Orientation(d90, -d30, 0));

        square1.getPolygons().forEach(p -> p.setPaint(Paint.valueOf("772030")));
        square2.getPolygons().forEach(p -> p.setPaint(Paint.valueOf("302077")));

        chunkDataStructure.addObject(square1);
        chunkDataStructure.addObject(square2);

    }

    public Object3D getSquare() {
        Object3D object3D = new MovingSquare();
        List<Polygon3D> polygons = new ArrayList<>();
        Polygon3D polygon1 = new Polygon3D(List.of(
                new Point3D(0, 150, 150),
                new Point3D(0, 150, -150),
                new Point3D(0, -150, 150)
        ));
        Polygon3D polygon2 = new Polygon3D(List.of(
                new Point3D(0, -150, -150),
                new Point3D(0, -150, 150),
                new Point3D(0, 150, -150)
        ));
        Polygon3D polygon3 = new Polygon3D(List.of(
                new Point3D(0, 150, -150),
                new Point3D(0, 150, 150),
                new Point3D(0, -150, 150)
        ));
        Polygon3D polygon4 = new Polygon3D(List.of(
                new Point3D(0, -150, 150),
                new Point3D(0, -150, -150),
                new Point3D(0, 150, -150)
        ));

        polygons.add(polygon1);
        polygons.add(polygon2);
        polygons.add(polygon3);
        polygons.add(polygon4);

        polygons.forEach(p -> p.setPaint(Paint.valueOf("503020")));
        object3D.setPolygons(polygons);

        return object3D;
    }

    @Override
    public String getName() {
        return "intersecting-moving-squares";
    }

    public static class MovingSquare extends Object3D {

        {
            this.speedVector = new Point3D(0, 1, 0);
        }

        @Override
        public void processObject() {
            double ySpeed = 1;
            super.processObject();
            if (this.location.getY() > 150) {
                this.speedVector = new Point3D(0, -ySpeed, 0);
            } else if (this.location.getY() < -150) {
                this.speedVector = new Point3D(0, ySpeed, 0);
            }
        }
    }

}
