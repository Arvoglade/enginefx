package ru.arvoglade.Engine.engineCore.scene;

import javafx.geometry.Point3D;
import javafx.scene.paint.Paint;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import ru.arvoglade.Engine.model.Object3D;
import ru.arvoglade.Engine.model.Orientation;
import ru.arvoglade.Engine.model.Polygon3D;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class SceneBuilderConfig {

    @Bean("one")
    @Scope("prototype")
    public BeanEngineScene getSceneOneObject() {
        return new BeanEngineScene() {
            @Override
            public String getName() {
                return "one";
            }

            {
                cameraSettings.setLocation(new Point3D(-300, 0, 0));
                cameraSettings.setOrientation(new Orientation(0, 0, 0));

                Object3D object = getTetrahedron();

                object.setLocation(new Point3D(300, 0, 0));
                //object.setOrientation(new Orientation( 180 * Math.PI / 180, 45 * Math.PI / 180, 0));

                object.setRotationAxleOrientation(new Orientation(0, 90 * Math.PI / 180, 0));
                object.setRotationComponent(new Orientation(0, 0, 0.05));

                chunkDataStructure.addObject(object);
            }
        };
    }

    @Bean("three")
    @Scope("prototype")
    public BeanEngineScene getScene3Objects() {

        return new BeanEngineScene() {
            @Override
            public String getName() {
                return "three";
            }

            {
                cameraSettings.setLocation(new Point3D(-300, 0, 0));
                cameraSettings.setOrientation(new Orientation(0, 0, 0));

                Object3D object = getTetrahedron();
                Object3D object2 = getTetrahedron();
                Object3D object3 = getTetrahedron();


                object2.setLocation(new Point3D(200, 0, -150));
                object3.setLocation(new Point3D(200, 0, 150));

                object.setLocation(new Point3D(300, 0, 0));
                object.setOrientation(new Orientation( 0, 0, 0));

                chunkDataStructure.addObject(object);
                chunkDataStructure.addObject(object2);
                chunkDataStructure.addObject(object3);

            }
        };
    }

    @Bean("sphere")
    @Scope("prototype")
    public BeanEngineScene getSceneSphere() {

        return new BeanEngineScene() {
            @Override
            public String getName() {
                return "sphere";
            }

            {
                cameraSettings.setLocation(new Point3D(-300, 0, 0));
                cameraSettings.setOrientation(new Orientation(0, 0, 0));

                double al;
                double phi;
                double r = 800;

                int maxCount = 12;
                for (int i = -90; i <= 90; i += 20) {
                    int thisStepCount = (int) (maxCount * Math.cos(((double)i) * Math.PI / 180));
                    for (int j = 0; j < thisStepCount; j += 1) {
                        if (i == -90) {
                            al = 0;
                            phi = -90;
                        } else if (i == 90) {
                            al = 0;
                            phi = 90;
                        } else {
                            al = 360. / thisStepCount * j;
                            phi = i;
                        }

                        double x = r * Math.cos(al * Math.PI / 180) * Math.cos(phi * Math.PI / 180);
                        double y = r * Math.sin(al * Math.PI / 180) * Math.cos(phi * Math.PI / 180);
                        double z = r * Math.sin(phi * Math.PI / 180);

                        Object3D obj = getTetrahedron();
                        obj.setLocation(new Point3D(x, y, z));
                        //obj.setOrientation(new Orientation(0, -Math.PI / 2, 0));

                        obj.setRotationAxleOrientation(new Orientation(0, 0, 0));
                        obj.setRotationComponent(new Orientation(0.1, 0.0, 0));

                        chunkDataStructure.addObject(obj);
                    }

                }

                List<Object3D> skyBlocks = getSkyBlock();
                skyBlocks.forEach((sb) -> chunkDataStructure.addObject(sb));
            }
        };
    }

    public double rndRadian() {
        return Math.random() * 2 * Math.PI - Math.PI;
    }

    private List<Object3D> getSkyBlock() {
        Object3D skyBlock = new Object3D();
        double p = 1_000_000_000_000.;
        double n = -p;

        Point3D upLeftF = new Point3D(p, p, p);
        Point3D upLeftB = new Point3D(n, p, p);
        Point3D upRightF = new Point3D(p, n, p);
        Point3D upRightB = new Point3D(n, n, p);

        Point3D downLeftF = new Point3D(p, p, n);
        Point3D downLeftB = new Point3D(n, p, n);
        Point3D downRightF = new Point3D(p, n, n);
        Point3D downRightB = new Point3D(n, n, n);

        Polygon3D up = new Polygon3D(
                upLeftF,
                upRightF,
                upRightB,
                upLeftB
        );
        Polygon3D down = new Polygon3D(
                downRightF,
                downLeftF,
                downLeftB,
                downRightB
        );
        Polygon3D left = new Polygon3D(
                upLeftF,
                upLeftB,
                downLeftB,
                downLeftF
        );
        Polygon3D right = new Polygon3D(
                upRightB,
                upRightF,
                downRightF,
                downRightB
        );
        Polygon3D forward = new Polygon3D(
                upRightF,
                upLeftF,
                downLeftF,
                downRightF
        );
        Polygon3D backward = new Polygon3D(
                upRightB,
                downRightB,
                downLeftB,
                upLeftB
        );

        List<Polygon3D> polygon3DS = new ArrayList<>();
        polygon3DS.add(up);
        polygon3DS.add(down);
        polygon3DS.add(left);
        polygon3DS.add(right);
        polygon3DS.add(forward);
        polygon3DS.add(backward);

        Paint paint = Paint.valueOf("000000");
        polygon3DS.forEach(polygon -> polygon.setPaint(paint));

        List<Object3D> skyBlocks = new ArrayList<>();
        skyBlocks.add(new Object3D(List.of(up)));
        skyBlocks.add(new Object3D(List.of(down)));
        skyBlocks.add(new Object3D(List.of(forward)));
        skyBlocks.add(new Object3D(List.of(backward)));
        skyBlocks.add(new Object3D(List.of(left)));
        skyBlocks.add(new Object3D(List.of(right)));

        return skyBlocks;
    }

    private Object3D getObject() {
        Polygon3D polygon = new Polygon3D(
                new Point3D(60, -40, 0),
                new Point3D(-60, -40, 0),
                new Point3D(0, 80, 0));
        polygon.setPaint(Paint.valueOf("01aa01"));

        Object3D obj = new Object3D(List.of(polygon));
        return obj;
    }

    public Object3D getTetrahedron() {

        List<Polygon3D> polygons = new ArrayList<>();
        Polygon3D polygon1 = new Polygon3D(
                new Point3D(0, 0, 150),
                new Point3D(60, -40, 0),
                new Point3D(0, 80, 0));
        Polygon3D polygon2 = new Polygon3D(
                new Point3D(-60, -40, 0),
                new Point3D(60 , -40, 0),
                new Point3D(0, 0, 150));
        Polygon3D polygon3 = new Polygon3D(
                new Point3D(0, 80, 0),
                new Point3D(-60, -40, 0),
                new Point3D(0, 0, 150));
        Polygon3D polygon4 = new Polygon3D(
                new Point3D(60, -40, 0),
                new Point3D(-60, -40, 0),
                new Point3D(0, 80, 0));

        polygon1.setPaint(Paint.valueOf("ff0000"));
        polygon2.setPaint(Paint.valueOf("00ff00"));
        polygon3.setPaint(Paint.valueOf("0000ff"));
        polygon4.setPaint(Paint.valueOf("ff00ff"));

        polygons.add(polygon1);
        polygons.add(polygon2);
        polygons.add(polygon3);
        polygons.add(polygon4);

        Point3D location = new Point3D(200, 200, 0);
        Point3D center = new Point3D(0, 0, 60);
        return new Object3D(polygons, location, center);
    }
}
