package ru.arvoglade.Engine.engineCore.scene;

import ru.arvoglade.Engine.engineCore.chunk.ChunkDataStructure;
import ru.arvoglade.Engine.process.Camera;

public abstract class BeanEngineScene implements EngineScene {

    protected ChunkDataStructure chunkDataStructure;

    protected Camera cameraSettings;

    public BeanEngineScene() {
        cameraSettings = new Camera();
        chunkDataStructure = new ChunkDataStructure(1, 500);
    }

    @Override
    public ChunkDataStructure getChunkDataStructure() {
        return chunkDataStructure;
    }

    @Override
    public Camera getCameraSettingsDefault() {
        return cameraSettings;
    }


    @Override
    public Camera getCameraSettingsForUser(String id) {
        return cameraSettings;
    }

    @Override
    public void loadForChunk(String chunkId) {

    }

    @Override
    public void loadForUser(String userId) {

    }

    @Override
    public void unloadForChunk(String chunkId) {

    }

    @Override
    public void unloadExceptChunk(String chunkId) {

    }
}
