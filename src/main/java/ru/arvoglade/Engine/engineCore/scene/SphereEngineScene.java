package ru.arvoglade.Engine.engineCore.scene;

import javafx.geometry.Point3D;
import javafx.scene.paint.Paint;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.arvoglade.Engine.engineCore.chunk.ChunkDataStructure;
import ru.arvoglade.Engine.model.Object3D;
import ru.arvoglade.Engine.model.Orientation;
import ru.arvoglade.Engine.model.Polygon3D;
import ru.arvoglade.Engine.process.Camera;

import java.util.ArrayList;
import java.util.List;

@Component("sphere2")
@Scope("prototype")
public class SphereEngineScene extends BeanEngineScene {

    public SphereEngineScene() {
        cameraSettings = new Camera();
        cameraSettings.setLocation(new Point3D(-300, 0, 0));
        cameraSettings.setOrientation(new Orientation(0, 0, 0));
        chunkDataStructure = new ChunkDataStructure(1, 500);

        double al;
        double phi;
        double r = 800;

        int maxCount = 12;
        for (int i = -90; i <= 90; i += 20) {
            int thisStepCount = (int) (maxCount * Math.cos(((double)i) * Math.PI / 180));
            for (int j = 0; j < thisStepCount; j += 1) {
                if (i == -90) {
                    al = 0;
                    phi = -90;
                } else if (i == 90) {
                    al = 0;
                    phi = 90;
                } else {
                    al = 360. / thisStepCount * j;
                    phi = i;
                }

                double x = r * Math.cos(al * Math.PI / 180) * Math.cos(phi * Math.PI / 180);
                double y = r * Math.sin(al * Math.PI / 180) * Math.cos(phi * Math.PI / 180);
                double z = r * Math.sin(phi * Math.PI / 180);

                Object3D obj = getTetrahedron();
                obj.setLocation(new Point3D(x, y, z));

                obj.setRotationAxleOrientation(new Orientation(0, 0, 0));
                obj.setRotationComponent(new Orientation(0.1, 0.0, 0));

                chunkDataStructure.addObject(obj);
            }

        }
    }

    public static Object3D getTetrahedron() {

        List<Polygon3D> polygons = new ArrayList<>();
        Polygon3D polygon1 = new Polygon3D(
                new Point3D(0, 0, 150),
                new Point3D(60, -40, 0),
                new Point3D(0, 80, 0));
        Polygon3D polygon2 = new Polygon3D(
                new Point3D(-60, -40, 0),
                new Point3D(60 , -40, 0),
                new Point3D(0, 0, 150));
        Polygon3D polygon3 = new Polygon3D(
                new Point3D(0, 80, 0),
                new Point3D(-60, -40, 0),
                new Point3D(0, 0, 150));
        Polygon3D polygon4 = new Polygon3D(
                new Point3D(60, -40, 0),
                new Point3D(-60, -40, 0),
                new Point3D(0, 80, 0));

        polygon1.setPaint(Paint.valueOf("ff0000"));
        polygon2.setPaint(Paint.valueOf("00ff00"));
        polygon3.setPaint(Paint.valueOf("0000ff"));
        polygon4.setPaint(Paint.valueOf("ff00ff"));

        polygons.add(polygon1);
        polygons.add(polygon2);
        polygons.add(polygon3);
        polygons.add(polygon4);

        Point3D location = new Point3D(200, 200, 0);
        Point3D center = new Point3D(0, 0, 60);
        return new Object3D(polygons, location, center);
    }

    @Override
    public String getName() {
        return "sphere2";
    }

    @Override
    public Camera getCameraSettingsForUser(String id) {
        return cameraSettings;
    }

    @Override
    public void loadForChunk(String chunkId) {

    }

    @Override
    public void loadForUser(String userId) {

    }

    @Override
    public void unloadForChunk(String chunkId) {

    }

    @Override
    public void unloadExceptChunk(String chunkId) {

    }

}
