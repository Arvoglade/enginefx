package ru.arvoglade.Engine.engineCore.scene;

import javafx.geometry.Point3D;
import javafx.scene.paint.Paint;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.arvoglade.Engine.model.Object3D;
import ru.arvoglade.Engine.model.Polygon3D;

import java.util.ArrayList;
import java.util.List;

@Component("square")
@Scope("prototype")
public class SquareEngineScene extends BeanEngineScene {

    public SquareEngineScene() {

        Object3D object3D = new Object3D();
        object3D.setLocation(new Point3D(300, 0, 0));

        List<Polygon3D> polygons = new ArrayList<>();
        Polygon3D polygon1 = new Polygon3D(List.of(
                new Point3D(0, 150, 150),
                new Point3D(0, 150, -150),
                new Point3D(0, -150, 150)
                ));
        Polygon3D polygon2 = new Polygon3D(List.of(
                new Point3D(0, -150, -150),
                new Point3D(0, -150, 150),
                new Point3D(0, 150, -150)
        ));
        polygons.add(polygon1);
        polygons.add(polygon2);

        polygons.forEach(p -> p.setPaint(Paint.valueOf("503020")));
        object3D.setPolygons(polygons);

        List<Polygon3D> polygonsBack = new ArrayList<>();
        Polygon3D polygon3 = new Polygon3D(List.of(
                new Point3D(0, 150, -150),
                new Point3D(0, 150, 150),
                new Point3D(0, -150, 150)
        ));
        Polygon3D polygon4 = new Polygon3D(List.of(
                new Point3D(0, -150, 150),
                new Point3D(0, -150, -150),
                new Point3D(0, 150, -150)
        ));
        polygonsBack.add(polygon4);
        polygonsBack.add(polygon3);
        polygonsBack.forEach(p -> p.setPaint(Paint.valueOf("203050")));

        Object3D object3D2 = new Object3D();
        object3D2.setLocation(new Point3D(300, 0, 0));
        object3D2.setPolygons(polygonsBack);

        chunkDataStructure.addObject(object3D);
        chunkDataStructure.addObject(object3D2);
    }

    @Override
    public String getName() {
        return "square";
    }
}
