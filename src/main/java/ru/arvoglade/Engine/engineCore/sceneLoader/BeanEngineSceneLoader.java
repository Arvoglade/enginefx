package ru.arvoglade.Engine.engineCore.sceneLoader;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import ru.arvoglade.Engine.engineCore.scene.BeanEngineScene;
import ru.arvoglade.Engine.engineCore.scene.EngineScene;

import java.util.Arrays;
import java.util.List;

@Component
public class BeanEngineSceneLoader implements EngineSceneLoader, ApplicationContextAware {

    private List<String> beanEngineScenesNames;

    private ApplicationContext applicationContext;

    @Override
    public EngineScene getEngineScene(String name) {
        return applicationContext.getBean(name, BeanEngineScene.class);
    }

    @Override
    public List<String> getEngineScenesNamesList() {
        return Arrays.stream(applicationContext.getBeanNamesForType(BeanEngineScene.class)).toList();
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }


}
