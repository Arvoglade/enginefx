package ru.arvoglade.Engine.engineCore.sceneLoader;

import ru.arvoglade.Engine.engineCore.scene.EngineScene;

import java.util.List;

public interface EngineSceneLoader {

    EngineScene getEngineScene(String name);

    List<String> getEngineScenesNamesList();

}
