package ru.arvoglade.Engine.engineCore.chunk;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ChunkConfig {

    @Bean
    public ChunkDataStructure getChunkDataStructure() {
        return new ChunkDataStructure(1, 500);
    }

}
