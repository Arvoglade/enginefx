package ru.arvoglade.Engine.engineCore.chunk;

import javafx.geometry.Point3D;
import lombok.Getter;
import ru.arvoglade.Engine.model.Object3D;

import java.util.HashMap;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

public class ChunkDataStructure {

    private ConcurrentHashMap<String, ChunkEntry> entriesByIdMap;

    @Getter
    private ChunkNode rootChunkNode;

    public ChunkDataStructure(int initialChunkLevel, double initialChunkSize, Point3D initialChunkLocation, int subchunkLinearCount) {
        if (initialChunkLevel < 1)
            throw new IllegalArgumentException("initialChunkLevel shouldn't bu less than 1");

        if (subchunkLinearCount < 3)
            throw new IllegalArgumentException("subchunks linear count shouldn't be less than 3");

        if (subchunkLinearCount % 2 == 0)
            subchunkLinearCount++;

        String initialChunkId = initialChunkLevel + ":0,0,0;";

        rootChunkNode = new ChunkNode(initialChunkId, initialChunkLevel, initialChunkSize, initialChunkLocation, subchunkLinearCount);
        this.entriesByIdMap = new ConcurrentHashMap<>();
    }

    public ChunkDataStructure(int initialChunkLevel, double initialChunkSize, Point3D initialChunkLocation) {
        this(initialChunkLevel, initialChunkSize, initialChunkLocation, 5);
    }

    public ChunkDataStructure(int initialChunkLevel, double initialChunkSize) {
        this(initialChunkLevel, initialChunkSize, Point3D.ZERO);
    }

    public Stream<Object3D> getAllObjects() {
        return entriesByIdMap.values().stream().map(ChunkEntry::get).filter(Objects::nonNull);
    }

    public Object3D getById(String id) {
        return entriesByIdMap.get(id).get();
    }

    public ChunkEntry getEntryById(String id) {
        return entriesByIdMap.get(id);
    }

    public void addObject(Object3D object3D) {
        addElement(new ChunkEntry(object3D));
    }

    public void addElement(ChunkEntry chunkEntry) {
        entriesByIdMap.put(chunkEntry.getId(), chunkEntry);
        while (!rootChunkNode.isInBound(chunkEntry)) {
            String newRootId = (rootChunkNode.getChunkLevel() + 1) + ":0,0,0;";
            ChunkNode newRoot = new ChunkNode(
                    newRootId,
                    rootChunkNode.getChunkLevel() + 1,
                    rootChunkNode.getSize() * rootChunkNode.getSubchunkLinearCount(),
                    rootChunkNode.getLocation(),
                    rootChunkNode.getSubchunkLinearCount()
                    );
            newRoot.getSubchunks().put(rootChunkNode.getId(), rootChunkNode);
            rootChunkNode.setParentChunk(newRoot);
            rootChunkNode = newRoot;
        }
        rootChunkNode.addElement(chunkEntry);
    }

    public void deleteEntry(String id) {
        ChunkEntry entry = entriesByIdMap.get(id);
        entry.set(null);
        entry.getParentChunk().deleteEntry(id);
    }

    public void moveEntry(String id) {
        ChunkEntry entry = entriesByIdMap.get(id);
        ChunkNode oldChunk = entry.getParentChunk();

        addElement(entry);

        if (entry.getParentChunk() != oldChunk) {
            oldChunk.deleteEntry(id);
        }
    }

}
