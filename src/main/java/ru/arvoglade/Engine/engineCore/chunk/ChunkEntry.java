package ru.arvoglade.Engine.engineCore.chunk;

import javafx.geometry.Point3D;
import ru.arvoglade.Engine.model.Object3D;

public class ChunkEntry {

    private Object3D object3D;

    private ChunkNode chunkNode;

    public ChunkEntry(Object3D object3D) {
        this.object3D = object3D;
    }

    public Point3D getLocation() {
        return object3D.getLocation();
    }

    public String getId() {
        return object3D.getId();
    }

    public Object3D get() {
        return object3D;
    }

    public void set(Object3D object3D) {
        this.object3D = object3D;
    }

    public void setParentChunk(ChunkNode chunk) {
        this.chunkNode = chunk;
    }

    public ChunkNode getParentChunk() {
        return chunkNode;
    }
}
