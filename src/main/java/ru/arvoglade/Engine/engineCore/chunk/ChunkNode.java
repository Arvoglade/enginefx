package ru.arvoglade.Engine.engineCore.chunk;

import javafx.geometry.Point3D;
import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

public class ChunkNode {

    @Getter
    private int chunkLevel;

    @Getter
    private final Point3D location;

    @Getter
    private final double size;

    @Getter
    private final int subchunkLinearCount;

    @Getter
    private ConcurrentHashMap<String, ChunkEntry> elements;

    @Getter
    private ConcurrentHashMap<String, ChunkNode> subchunks;

    @Getter @Setter
    private ChunkNode parentChunk;

    private String chunkId;

    ChunkNode(String chunkId, int chunkLevel, double size, Point3D location, int subchunkLinearCount) {
        this.chunkLevel = chunkLevel;
        this.location = location;
        this.size = size;
        this.chunkId = chunkId;
        this.subchunkLinearCount = subchunkLinearCount;
        this.elements = new ConcurrentHashMap<>();
        this.subchunks = new ConcurrentHashMap<>();

    }

    public Stream<ChunkEntry> getAll() {
        if (chunkLevel == 0) {
            return elements.values().stream().filter(entry -> entry.get() != null);
        } else {
            return subchunks.values().stream().flatMap(ChunkNode::getAll).filter(entry -> entry.get() != null);
        }
    }

    public Stream<ChunkEntry> getAllEntriesStream() {
        if (chunkLevel == 0) {
            return elements.values().stream();
        } else {
            return subchunks.values().stream().flatMap(ChunkNode::getAllEntriesStream);
        }
    }

    public void addElement(ChunkEntry entry) {

        if (chunkLevel == 0) {
            entry.setParentChunk(this);
            elements.put(entry.getId(), entry);
        } else {
            int[] position = computeSubchunkRelativePosition(entry.getLocation());
            String subchunkID = computeSubchunkId(position);
            if (!subchunks.containsKey(subchunkID)) {
                subchunks.put(subchunkID, createSubchunk(subchunkID, position));
            }
            subchunks.get(subchunkID).addElement(entry);
        }
    }

    public void deleteEntry(String id) {
        elements.remove(id);
        if (elements.size() == 0 && parentChunk != null) {
            parentChunk.deleteChunk(this.chunkId);
        }
    }

    public void deleteChunk(String id) {
        subchunks.remove(id);
        if (subchunks.size() == 0 && parentChunk != null) {
            parentChunk.deleteChunk(this.chunkId);
        }
    }

    int[] computeSubchunkRelativePosition(Point3D randomInnerPoint) {
        int[] subchunkPosition = new int[3];
        Point3D relativeLocation = randomInnerPoint.subtract(location);

        double subchunkSize = size / subchunkLinearCount;
        subchunkPosition[0] = (int) Math.floor(relativeLocation.getX() / subchunkSize + 0.5);
        subchunkPosition[1] = (int) Math.floor(relativeLocation.getY() / subchunkSize + 0.5);
        subchunkPosition[2] = (int) Math.floor(relativeLocation.getZ() / subchunkSize + 0.5);

        return subchunkPosition;
    }

    String computeSubchunkId(int[] subchunkPosition) {
        String[] thisChunkIdElements = chunkId.split(":");
        String lastIdElement = thisChunkIdElements[thisChunkIdElements.length - 1];

        String thisChunkId;
        if (lastIdElement.charAt(0) != '0'
                || lastIdElement.charAt(2) != '0'
                || lastIdElement.charAt(4) != '0')
            thisChunkId = chunkId;
        else
            thisChunkId = "";

        String localSubchunkId = thisChunkId + (chunkLevel - 1) +
                ":" +
                subchunkPosition[0] +
                "," +
                subchunkPosition[1] +
                "," +
                subchunkPosition[2] +
                ";";
        return localSubchunkId;
    }

    // ??
    int[] computePositionsFromChunkIdElement(String chunkId) {
        int index = chunkId.indexOf(':');
        String strPositions = chunkId.substring(index + 1, chunkId.length() - 2);
        return Arrays.stream(strPositions.split(",")).mapToInt(Integer::parseInt).toArray();
    }

    ChunkNode createSubchunk(String subchunkID, int[] subchunkPosition) {
        double subchunkSize = size / subchunkLinearCount;

        Point3D subchunkRelativeLocation = new Point3D(
                subchunkPosition[0] * subchunkSize,
                subchunkPosition[1] * subchunkSize,
                subchunkPosition[2] * subchunkSize);
        Point3D subchunkLocation = subchunkRelativeLocation.add(location);
        ChunkNode subchunk = new ChunkNode(
                subchunkID,
                chunkLevel - 1,
                subchunkSize,
                subchunkLocation,
                subchunkLinearCount);
        subchunk.setParentChunk(this);
        return subchunk;
    }

    public double getMinX() {
        return location.getX() - size / 2;
    }

    public double getMaxX() {
        return location.getX() + size / 2;
    }

    public double getMinY() {
        return location.getY() - size / 2;
    }

    public double getMaxY() {
        return location.getY() + size / 2;
    }

    public double getMinZ() {
        return location.getZ() - size / 2;
    }

    public double getMaxZ() {
        return location.getZ() + size / 2;
    }

    public String getId() {
        return chunkId;
    }

    public boolean isInBound(ChunkEntry chunkEntry) {
        Point3D p = chunkEntry.getLocation();
        return      p.getX() > getMinX() && p.getX() < getMaxX()
                &&  p.getY() > getMinY() && p.getY() < getMaxY()
                &&  p.getZ() > getMinZ() && p.getZ() < getMaxZ();
    }

    public List<Point3D> getVertexes() {
        return List.of(
                new Point3D(getMinX(), getMinY(), getMinZ()),
                new Point3D(getMaxX(), getMinY(), getMinZ()),
                new Point3D(getMinX(), getMaxY(), getMinZ()),
                new Point3D(getMaxX(), getMaxY(), getMinZ()),

                new Point3D(getMinX(), getMinY(), getMaxZ()),
                new Point3D(getMaxX(), getMinY(), getMaxZ()),
                new Point3D(getMinX(), getMaxY(), getMaxZ()),
                new Point3D(getMaxX(), getMaxY(), getMaxZ())
        );
    }
}
