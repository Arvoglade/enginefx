package ru.arvoglade.Engine.engineCore.engineProcessor.physics;

import javafx.geometry.Point3D;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.arvoglade.Engine.engineCore.engineProcessor.EngineProcessor;
import ru.arvoglade.Engine.engineCore.chunk.ChunkDataStructure;
import ru.arvoglade.Engine.model.Object3D;

@Component
public class GravitationPhysicsProcessor implements EngineProcessor {

    @Value("${physics.gravitationalConst}")
    private double FORCE = 1000;

    @Override
    public void process(ChunkDataStructure chunkDataStructure) {
        chunkDataStructure.getAllObjects()
                .forEach((obj1) -> chunkDataStructure.getAllObjects()
                        .filter(obj2 -> obj2 != obj1)
                        .forEach(obj2 -> interact(obj1, obj2)));
    }

    @Override
    public double getOrder() {
        return 0;
    }

    public void interact(Object3D obj1, Object3D obj2) {
        double r = obj1.getLocation().distance(obj2.getLocation());
        if (r < 100) {
            r = 100;
        }
        double a = FORCE * obj2.getMass() / r / r;

        double ax = (obj2.getLocation().getX() - obj1.getLocation().getX()) / r * a;
        double ay = (obj2.getLocation().getY() - obj1.getLocation().getY()) / r * a;
        double az = (obj2.getLocation().getZ() - obj1.getLocation().getZ()) / r * a;

        obj1.setSpeedVector(obj1.getSpeedVector().add(new Point3D(ax, ay, az)));
    }

}
