package ru.arvoglade.Engine.engineCore.engineProcessor.drawing;

import jakarta.annotation.Resource;
import javafx.scene.shape.Polygon;
import org.springframework.stereotype.Component;
import ru.arvoglade.Engine.engineCore.engineProcessor.EngineProcessor;
import ru.arvoglade.Engine.engineCore.chunk.ChunkDataStructure;
import ru.arvoglade.Engine.model.Object3D;
import ru.arvoglade.Engine.process.Camera;
import ru.arvoglade.Engine.process.Projector;
import ru.arvoglade.Engine.process.drawer.Drawer;

import java.util.List;

@Component
public class DrawingProcessor implements EngineProcessor {


    @Resource(name = "${drawer}")
    private Drawer drawer;
    private Projector projector;

    private FrustumCulling frustumCulling;

    private Camera camera;

    public DrawingProcessor(Projector projector, Camera camera, FrustumCulling frustumCulling) {
        this.projector = projector;
        this.camera = camera;
        this.frustumCulling = frustumCulling;
    }

    @Override
    public void process(ChunkDataStructure chunkDataStructure) {
        List<Object3D> objects = frustumCulling.frustumCulling(camera, chunkDataStructure);
        List<Polygon> polygons = projector.getSceneProjection(camera, objects);
        drawer.draw(polygons);
    }

    @Override
    public double getOrder() {
        return 0;
    }


}
