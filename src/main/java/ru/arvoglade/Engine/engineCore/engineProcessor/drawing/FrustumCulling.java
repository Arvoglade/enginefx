package ru.arvoglade.Engine.engineCore.engineProcessor.drawing;

import javafx.geometry.Point3D;
import org.springframework.stereotype.Component;
import ru.arvoglade.Engine.engineCore.chunk.ChunkDataStructure;
import ru.arvoglade.Engine.engineCore.chunk.ChunkEntry;
import ru.arvoglade.Engine.engineCore.chunk.ChunkNode;
import ru.arvoglade.Engine.model.Object3D;
import ru.arvoglade.Engine.process.Camera;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

import static java.lang.Math.PI;

@Component
public class FrustumCulling {

    private double halfViewAngle;

    private double viewDistance;
    private final double SCREEN_CORNER_VIEW_FIELD_MULTIPLIER = Math.sqrt(1 + Math.pow(1080. / 1920, 2));

    /**
     * Retrieves all 3D objects from chunkDataStructure,
     * that are visible from camera point of view
     * @param camera
     * @param chunkDataStructure
     * @return visible objects
     */
    public List<Object3D> frustumCulling(Camera camera, ChunkDataStructure chunkDataStructure) {
        halfViewAngle = camera.getViewField() / 2 * SCREEN_CORNER_VIEW_FIELD_MULTIPLIER;
        viewDistance = camera.getViewDistance();
        if (viewDistance == 0)
            viewDistance = Double.POSITIVE_INFINITY;
        ChunkNode rootNode = chunkDataStructure.getRootChunkNode();
        Stream<ChunkNode> visibleChunks = getVisibleNodes(camera, rootNode);
        return visibleChunks.flatMap(ChunkNode::getAll).map(ChunkEntry::get).filter(Objects::nonNull).toList();
    }

    /**
     * Recursively retrieves all visible chunks.
     * @param camera
     * @param chunk
     * @return
     */
    private Stream<ChunkNode> getVisibleNodes(Camera camera, ChunkNode chunk) {
        int viewResult = isChunkInField(camera, chunk);
        if (viewResult == -1) {
            return Stream.empty();
        } else if (viewResult == 1) {
            return Stream.of(chunk);
        } else if (viewResult == 0 && chunk.getChunkLevel() == 0) {
            return Stream.of(chunk);
        } else {
            return chunk.getSubchunks().values().stream().parallel().flatMap(c -> getVisibleNodes(camera, c));
        }
    }

    /**
     * Compute whether
     * all chunk entry is in view frustum,
     * or chunk partly in view frustum,
     * or chunk is  fully not in view frustum.
     * View frustum is bounded by field of view angle and viewDistance
     * @param camera
     * @param chunkNode
     * @return 1 if fully visible, 0 if partially visible, -1 if fully invisible
     */
    private int isChunkInField(Camera camera, ChunkNode chunkNode) {
        boolean fullyInvisible = true;
        boolean fullyVisible = true;
        boolean behindCamera = true;

        // Distance check
        double chunkCenterDistance = camera.getLocation().distance(chunkNode.getLocation());
        if (chunkCenterDistance - chunkNode.getSize() / 2 > viewDistance)
            return -1;
        else if (chunkCenterDistance + chunkNode.getSize() / 2 > viewDistance) {
            fullyVisible = false;
        }

        // View angle check
        List<Point3D> chunkVertexes = chunkNode.getVertexes();
        List<Point3D> chunkVertexesFromCamera = new ArrayList<>();
        for (Point3D p : chunkVertexes) {

            Point3D pointFromCamera = pointFromCamera(camera, p);
            chunkVertexesFromCamera.add(pointFromCamera);

            if (pointFromCamera.getX() < 0) {
                fullyVisible = false;
            } else {
                behindCamera = false;
                Point3D cameraDirectionVector = new Point3D(1, 0, 0);
                double viewAngle = this.halfViewAngle * 180 / PI;
                if (pointFromCamera.angle(cameraDirectionVector) < viewAngle) {
                    fullyInvisible = false;
                } else {
                    fullyVisible = false;
                }
            }
        }
        if (behindCamera) {
            return -1;
        }
        if (fullyVisible) {
            return 1;
        }
        if (!fullyInvisible) {
            return 0;
        }

        boolean partialVisibility = partialVisibilityCheck(chunkVertexesFromCamera);
        if (partialVisibility) {
            return 0;
        } else {
            return -1;
        }
    }

    /**
     * Computes whether camera sees entry of chunk
     * if its vertexes are not in field of view
     * @param chunkVertexesFromCamera - chunk vertexes in point of camera view
     * @return
     */
    private boolean partialVisibilityCheck(List<Point3D> chunkVertexesFromCamera) {
        double minHAngle = 1.5;
        double maxHAngle = -1.5;
        double minVAngle = 1.5;
        double maxVAngle = -1.5;
        for (Point3D p : chunkVertexesFromCamera) {

            double xz = Math.sqrt(p.getX() * p.getX() + p.getZ() * p.getZ());
            double hAngle = Math.atan2(p.getY(), xz);
            double xy = Math.sqrt(p.getX() * p.getX() + p.getY() * p.getY());
            double vAngle = Math.atan2(p.getZ(), xy);

            if (hAngle < minHAngle)
                minHAngle = hAngle;
            if (vAngle < minVAngle)
                minVAngle = vAngle;
            if (hAngle > maxHAngle)
                maxHAngle = hAngle;
            if (vAngle > maxVAngle)
                maxVAngle = vAngle;

        }
        boolean hMatch = false;
        boolean vMatch = false;
        if (minHAngle < 0 && maxHAngle > 0 ) {
            hMatch = true;
        } else  {
            if (minHAngle < 0) {
                minHAngle *= -1;
                maxHAngle *= -1;
            }
            if (halfViewAngle > minHAngle && halfViewAngle < maxHAngle)
                hMatch = true;
        }
        if (minVAngle < 0 && maxVAngle > 0 ) {
            vMatch = true;
        } else  {
            if (minVAngle < 0) {
                minVAngle *= -1;
                maxVAngle *= -1;
            }
            if (halfViewAngle > minVAngle && halfViewAngle < maxVAngle)
                vMatch = true;
        }
        return hMatch && vMatch;
    }

    private Point3D pointFromCamera(Camera camera, Point3D p) {
        return Object3D.getCoordinatesOfPointInAnotherCoordinateSystem(p,
                camera.getLocation(), camera.getOrientation());
    }

}
