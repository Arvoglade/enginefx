package ru.arvoglade.Engine.engineCore.engineProcessor;

import org.springframework.stereotype.Component;
import ru.arvoglade.Engine.engineCore.chunk.ChunkDataStructure;
import ru.arvoglade.Engine.process.Camera;


@Component
public class CameraMovingProcessor implements EngineProcessor {

    private Camera camera;

    public CameraMovingProcessor(Camera camera) {
        this.camera = camera;
    }

    @Override
    public void process(ChunkDataStructure objectsDataStructure) {
        camera.move();
    }

    @Override
    public double getOrder() {
        return 0;
    }
}
