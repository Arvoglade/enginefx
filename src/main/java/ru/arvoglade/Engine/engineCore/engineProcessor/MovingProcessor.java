package ru.arvoglade.Engine.engineCore.engineProcessor;

import org.springframework.stereotype.Component;
import ru.arvoglade.Engine.engineCore.chunk.ChunkDataStructure;

@Component
public class MovingProcessor implements EngineProcessor {
    @Override
    public void process(ChunkDataStructure chunkDataStructure) {
        chunkDataStructure.getAllObjects().forEach(obj -> {
            obj.processObject();
            chunkDataStructure.moveEntry(obj.getId());
        });
    }

    @Override
    public double getOrder() {
        return 0;
    }
}
