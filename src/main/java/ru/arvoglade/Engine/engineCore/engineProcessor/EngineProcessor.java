package ru.arvoglade.Engine.engineCore.engineProcessor;

import ru.arvoglade.Engine.engineCore.chunk.ChunkDataStructure;

public interface EngineProcessor {

    void process(ChunkDataStructure objectsDataStructure);

    double getOrder();

}
