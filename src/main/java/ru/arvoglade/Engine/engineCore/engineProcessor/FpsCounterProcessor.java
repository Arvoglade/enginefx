package ru.arvoglade.Engine.engineCore.engineProcessor;

import org.springframework.stereotype.Component;
import ru.arvoglade.Engine.engineCore.chunk.ChunkDataStructure;
import ru.arvoglade.Engine.process.FpsCounter;

@Component
public class FpsCounterProcessor implements EngineProcessor {

    private FpsCounter fpsCounter;

    private boolean started = false;

    public FpsCounterProcessor(FpsCounter fpsCounter) {
        this.fpsCounter = fpsCounter;
    }

    @Override
    public void process(ChunkDataStructure objectsDataStructure) {
        if (!started) {
            fpsCounter.start();
            started = true;
        }
        fpsCounter.increment();
    }

    @Override
    public double getOrder() {
        return 0;
    }
}
