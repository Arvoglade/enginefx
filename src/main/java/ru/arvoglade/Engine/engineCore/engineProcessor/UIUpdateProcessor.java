package ru.arvoglade.Engine.engineCore.engineProcessor;

import org.springframework.stereotype.Component;
import ru.arvoglade.Engine.engineCore.chunk.ChunkDataStructure;
import ru.arvoglade.Engine.process.UIupdater;

@Component
public class UIUpdateProcessor implements EngineProcessor {

    private final UIupdater uIupdater;

    public UIUpdateProcessor(UIupdater uIupdater) {
        this.uIupdater = uIupdater;
    }

    @Override
    public void process(ChunkDataStructure objectsDataStructure) {
        uIupdater.update();
    }

    @Override
    public double getOrder() {
        return 0;
    }
}
