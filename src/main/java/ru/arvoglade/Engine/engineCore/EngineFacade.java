package ru.arvoglade.Engine.engineCore;

import org.springframework.stereotype.Component;
import ru.arvoglade.Engine.engineCore.scene.EngineScene;
import ru.arvoglade.Engine.engineCore.sceneLoader.BeanEngineSceneLoader;

@Component
public class EngineFacade {

    private final EngineSceneRunner engineSceneRunner;

    private final BeanEngineSceneLoader beanEngineSceneLoader;

    public EngineFacade(EngineSceneRunner engineSceneRunner, BeanEngineSceneLoader beanEngineSceneLoader) {
        this.engineSceneRunner = engineSceneRunner;
        this.beanEngineSceneLoader = beanEngineSceneLoader;
    }

    public void startScene(String name) {
        EngineScene engineScene = beanEngineSceneLoader.getEngineScene(name);
        engineSceneRunner.setEngineScene(engineScene);
        engineSceneRunner.startScene();
    }

    public void stopScene() {
        engineSceneRunner.stopScene();
        engineSceneRunner.setEngineScene(null);
    }
}
