package ru.arvoglade.Engine.engineCore;

import org.springframework.stereotype.Component;

@Component
public class Chronometer {
    private int oneFrameRetentionMillis;
    private long timeLastFrame;

    public synchronized void waitOnChronometer() {
        long timeCurrent = System.currentTimeMillis();
        long diff = timeCurrent - timeLastFrame;
        if (diff < oneFrameRetentionMillis) {
            try {
                Thread.sleep(oneFrameRetentionMillis - diff);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        timeLastFrame = System.currentTimeMillis();
    }

    public void start(int targetFps) {
        int ONE_SECOND_MILLISECONDS = 1000;
        oneFrameRetentionMillis = ONE_SECOND_MILLISECONDS / targetFps;
        timeLastFrame = System.currentTimeMillis();
    }

    public void stop() {
    }

}
