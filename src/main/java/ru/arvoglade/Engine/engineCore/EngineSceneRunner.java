package ru.arvoglade.Engine.engineCore;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import ru.arvoglade.Engine.engineCore.engineProcessor.EngineProcessor;
import ru.arvoglade.Engine.engineCore.scene.EngineScene;
import ru.arvoglade.Engine.process.Camera;

import java.util.Comparator;
import java.util.List;

@Component
public class EngineSceneRunner {

    private final Chronometer chronometer;

    private Thread thread;

    private Camera camera;

    @Getter @Setter
    private EngineScene engineScene;

    private final List<EngineProcessor> processors;

    public EngineSceneRunner(Chronometer chronometer, List<EngineProcessor> processors, Camera camera) {
        this.camera = camera;
        this.chronometer = chronometer;
        this.processors = processors;
        processors.sort(Comparator.comparingDouble(EngineProcessor::getOrder));
    }

    public void startScene() {
        if (thread == null) {
            this.thread = new Thread(this::run);
            thread.start();
        }
    }

    public void stopScene() {
        if (thread != null && thread.isAlive()) {
            this.thread.interrupt();
            thread = null;
        }
    }

    public void run() {

        chronometer.start(60);

        // Another responsibility
        camera.setLocation(engineScene.getCameraSettingsDefault().getLocation());
        camera.setOrientation(engineScene.getCameraSettingsDefault().getOrientation());

        while (!thread.isInterrupted()) {
            processors.parallelStream()             // TODO set to parallel later
                    .forEach(processor -> processor.process(engineScene.getChunkDataStructure()));
            chronometer.waitOnChronometer();
        }
        chronometer.stop();
    }

}
