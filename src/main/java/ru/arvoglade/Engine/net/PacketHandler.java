package ru.arvoglade.Engine.net;

import java.net.DatagramPacket;

public interface PacketHandler {

    String getName();

    //Consumer<DatagramPacket> getHandler();

    Object handle(DatagramPacket datagramPacket);
}
