package ru.arvoglade.Engine.process.serverInteraction;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javafx.geometry.Point3D;
import ru.arvoglade.Engine.model.Orientation;
import ru.arvoglade.Engine.process.Camera;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ClientData {

    public String name;
    public String ip;
    public int port;

    public LocationPoint locationPoint;

    public Orientation orientation;

    public void setCameraLocation(Camera camera) {
        Point3D location = camera.getLocation();
        locationPoint = new LocationPoint();
        locationPoint.x = location.getX();
        locationPoint.y = location.getY();
        locationPoint.z = location.getZ();
    }

    public static class LocationPoint {
        public double x;
        public double y;
        public double z;
    }

}
