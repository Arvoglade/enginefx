package ru.arvoglade.Engine.process.serverInteraction;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.geometry.Point3D;
import javafx.scene.paint.Paint;
import lombok.Data;
import org.springframework.stereotype.Component;
import ru.arvoglade.Engine.model.Object3D;
import ru.arvoglade.Engine.model.Polygon3D;
import ru.arvoglade.Engine.net.SocketSender;
import ru.arvoglade.Engine.process.Camera;

import java.util.ArrayList;
import java.util.List;

@Data
@Component
public class ServerDataExchanger {

    private final SocketSender sender;

    private final Camera camera;

    private ObjectMapper objectMapper;

    private List<ClientData> otherClients;

    public ServerDataExchanger(SocketSender sender, Camera camera) {
        this.sender = sender;
        this.camera = camera;
        this.objectMapper = new ObjectMapper();
        this.otherClients = new ArrayList<>();
    }

    public void beginExchange() {

        String key = "enginefxData";

        ClientData clientData = new ClientData();
        clientData.setCameraLocation(camera);
        clientData.orientation = camera.getOrientation();

        try {
            String data = key + " " + objectMapper.writeValueAsString(clientData);
            sender.send(data.getBytes());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

    }

    public List<Object3D> getOtherPlayers() {
        List<Object3D> players = new ArrayList<>();

        otherClients.forEach(client -> {
            Object3D player = getPlayerModel();
            Point3D location = new Point3D(client.locationPoint.x, client.locationPoint.y, client.locationPoint.z);
            player.setLocation(location);
            player.setOrientation(client.orientation);
            players.add(player);
        });

        return players;
    }

    public void setOtherClientsData(List<ClientData> otherClients) {
        this.otherClients = otherClients;
    }

    public Object3D getPlayerModel() {

        double multiplier = 0.33;

        List<Polygon3D> polygons = new ArrayList<>();
        Polygon3D polygon1 = new Polygon3D(
                new Point3D(0, 0, 150),
                new Point3D(60, -40, 0),
                new Point3D(0, 80, 0));
        Polygon3D polygon2 = new Polygon3D(
                new Point3D(-60, -40, 0),
                new Point3D(60 , -40, 0),
                new Point3D(0, 0, 150));
        Polygon3D polygon3 = new Polygon3D(
                new Point3D(0, 80, 0),
                new Point3D(-60, -40, 0),
                new Point3D(0, 0, 150));
        Polygon3D polygon4 = new Polygon3D(
                new Point3D(60, -40, 0),
                new Point3D(-60, -40, 0),
                new Point3D(0, 80, 0));

        polygon1.setPaint(Paint.valueOf("ff0000"));
        polygon2.setPaint(Paint.valueOf("00ff00"));
        polygon3.setPaint(Paint.valueOf("0000ff"));
        polygon4.setPaint(Paint.valueOf("ff00ff"));

        polygons.add(polygon1);
        polygons.add(polygon2);
        polygons.add(polygon3);
        polygons.add(polygon4);

        Point3D location = new Point3D(200, 200, 0);
        Point3D center = new Point3D(0, 0, 60);

        Object3D player = new Object3D(polygons, location, center);



        player.setCenter(player.getCenter().multiply(multiplier));

        player.getPolygons().forEach(polygon -> {
            List<Point3D> newPoints = new ArrayList<>();
            polygon.getPoints().forEach(p -> {
                newPoints.add(p.multiply(multiplier));
            });
            polygon.setPoints(newPoints);
            polygon.setPaint(Paint.valueOf("333355"));
        });

        return player;
    }
}
