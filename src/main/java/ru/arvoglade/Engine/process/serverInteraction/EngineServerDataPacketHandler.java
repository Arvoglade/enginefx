package ru.arvoglade.Engine.process.serverInteraction;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import org.springframework.stereotype.Component;
import ru.arvoglade.Engine.net.PacketHandler;

import java.io.IOException;
import java.net.DatagramPacket;
import java.util.ArrayList;
import java.util.List;

@Component
public class EngineServerDataPacketHandler implements PacketHandler {

    private ObjectMapper objectMapper;

    private final ServerDataExchanger serverDataExchanger;

    public EngineServerDataPacketHandler(ServerDataExchanger serverDataExchanger) {
        this.serverDataExchanger = serverDataExchanger;
        this.objectMapper = new ObjectMapper();
    }

    @Override
    public String getName() {
        return "enginefxData";
    }

    @Override
    public Object handle(DatagramPacket datagramPacket) {

        String data = new String(datagramPacket.getData());
        data = data.substring(getName().length() + 1);

        List<ClientData> otherClients = new ArrayList<>();
        try {
            otherClients =  objectMapper.readValue(data, new TypeReference<>(){});
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        serverDataExchanger.setOtherClientsData(otherClients);

        return null;
    }
}
