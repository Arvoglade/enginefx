package ru.arvoglade.Engine.process.drawer;

import jakarta.annotation.PostConstruct;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.geometry.Point2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.PixelWriter;
import javafx.scene.shape.Polygon;
import org.springframework.stereotype.Component;
import ru.arvoglade.Engine.window.graphiscAnchorPane.graphicsCanvas.GraphicsCanvas;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component("new")
public class DrawerFxPixelBased implements Drawer {

    //@Value("${window.graphicsPaneSize.height}")
    private double height;
    //@Value("${window.graphicsPaneSize.width}")
    private double width;
    private final Canvas canvas;
    private final GraphicsContext gc;

    private int[] buffer;

    private final PixelWriter pw;

    public DrawerFxPixelBased(GraphicsCanvas canvas, GraphicsContext graphicsContext) {
        this.canvas = canvas;
        this.gc = graphicsContext;
        this.pw = gc.getPixelWriter();
        height = canvas.getHeight();
        width = canvas.getWidth();
    }

    @PostConstruct
    public void init() {
        buffer = new int[((int) ((height) * (width)))];
    }

    public void draw(List<Polygon> polygons) {
        Platform.runLater(() -> {
            Arrays.fill(buffer, getArgb("00000000"));
            //gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
            drawPolygonsInGc(polygons);
            drawPointer();
        });
    }

    private void drawPointer() {
        double heightCenter = height / 2;
        double widthCenter = width / 2;
        canvas.getGraphicsContext2D().strokeLine(widthCenter - 10, heightCenter, widthCenter + 10, heightCenter);
        canvas.getGraphicsContext2D().strokeLine(widthCenter, heightCenter - 10, widthCenter, heightCenter + 10);
    }

    private void drawPolygonsInGc(List<Polygon> polygons) {
        polygons.forEach(this::drawOnePolygon);
        pw.setPixels(0, 0, (int) width, (int) height, PixelFormat.getIntArgbInstance(), buffer, 0, (int) width);
    }

    private void drawOnePolygon(Polygon polygon) {
        int pointsCount = polygon.getPoints().size() / 2;
        double[] xArr = new double[pointsCount];
        double[] yArr = new double[pointsCount];
        // fillArrays(polygon, xArr, yArr);
//        gc.setFill(polygon.getFill());
//        gc.fillPolygon(xArr, yArr, pointsCount);
        String col = polygon.getFill().toString().substring(2, 8);

        fillPolygon2(getPointsListFromPolygon(polygon), col);

    }

/*    private void fillArrays(Polygon polygon, double[] xCoordinates, double[] yCoordinates) {
        List<Double> points = polygon.getPoints();
        for (int i = 1; i < points.size(); i += 2) {
            xCoordinates[(i - 1) / 2] = points.get(i - 1);
            yCoordinates[(i - 1) / 2] = points.get(i);
        }
    }*/

    private List<Point2D> getPointsListFromPolygon(Polygon polygon) {
        List<Point2D> pointsList = new ArrayList<>();
        ObservableList<Double> points = polygon.getPoints();

        for (int i = 0; i < points.size() - 1; i += 2) {
            pointsList.add(new Point2D(points.get(i), points.get(i + 1)));
        }

        return pointsList ;
    }

    private void fillPolygon2(List<Point2D> points, String color) {
        int size = points.size();
        int highestPointIndex = 0;
        double xMin = points.get(0).getX();
        double yMin = points.get(0).getY();
        double xMax = points.get(0).getX();
        double yMax = points.get(0).getY();
        for (int i = 1; i < size; i++) {
            Point2D p = points.get(i);
            double x = p.getX();
            double y = p.getY();
            if (y > yMax) {
                highestPointIndex = i;
                yMax = y;
            }
            if (y < yMin)
                yMin = y;
            if (x > xMax)
                xMax = x;
            if (x < xMin)
                xMin = x;
        }

        List<Point2D> pointsShifted = new ArrayList<>(size);
        pointsShifted.addAll(points.subList(highestPointIndex, size));
        pointsShifted.addAll(points.subList(0, highestPointIndex));

        List<Segment> segments = getSegments(pointsShifted);
        int argb = getArgb(color);

        yMin = Math.max(yMin, 0);
        yMax = Math.min(yMax, height);

        for (int row = (int) yMin; row < yMax; row++) {
            List<Double> xCrossingPoints = new ArrayList<>();
            for (int si = 0; si < segments.size(); si++) {
                Segment s = segments.get(si);
                if (s.a == 0) {
/*                    xCrossingPoints.add(s.xMin);
                    xCrossingPoints.add(s.xMax);*/
                    continue;
                }
                if (s.a == Double.POSITIVE_INFINITY || s.a == Double.NEGATIVE_INFINITY) {
                    xCrossingPoints.add(s.xMin);
                    continue;
                }
                if (row >= s.yMin && row <= s.yMax) {
                    xCrossingPoints.add((row - s.c) / s.a);
                }
            }

            xCrossingPoints = xCrossingPoints.stream().sorted().toList();

            for (int xcp = 0; xcp < xCrossingPoints.size() - 1; xcp += 2) {
                int xb = (int) (xCrossingPoints.get(xcp).doubleValue());
                int xe = (int) (xCrossingPoints.get(xcp + 1).doubleValue());
                xb = Math.max(xb, 0);
                xe = Math.min(xe, (int) width);
                for (int x = xb; x < xe; x++) {
                    //pw.setArgb(x, row, argb);
                    buffer[row * (int) width + x] = argb;
                }
            }
        }

    }

    public static int getArgb(String value) {
        if (value.length() <= 6)
            value = "ff" + value;
        long arbgLong = Long.parseLong(value, 16);
        if (arbgLong < 0) {
            return (int) (Integer.MAX_VALUE + arbgLong);
        } else {
            return (int) arbgLong;
        }
    }


    private List<Segment> getSegments(List<Point2D> pointsShifted) {
        int size = pointsShifted.size();
        List<Segment> segments = new ArrayList<>();
        for (int i = 0; i < size; i++) {

            int next = 0;
            if (i < size - 1) {
                next = i + 1;
            }

            Point2D p1 = pointsShifted.get(i);
            Point2D p2 = pointsShifted.get(next);
            segments.add(new Segment(p1, p2));
        }
        return segments;
    }

    public static class Segment {

        public Point2D p1;
        public Point2D p2;
        public double a;
        public double c;

        public double xMax;
        public double xMin;
        public double yMax;
        public double yMin;

        public Segment(Point2D p1, Point2D p2) {
            this.p1 = p1;
            this.p2 = p2;
            if (p2.getX() == p1.getX())
                a = Double.POSITIVE_INFINITY;
            else
                a = (p2.getY() - p1.getY()) / (p2.getX() - p1.getX());
            c = p1.getY() - a * p1.getX();

            xMax = Math.max(p1.getX(), p2.getX());
            xMin = Math.min(p1.getX(), p2.getX());
            yMax = Math.max(p1.getY(), p2.getY());
            yMin = Math.min(p1.getY(), p2.getY());
        }
    }

}
