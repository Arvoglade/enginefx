package ru.arvoglade.Engine.process.drawer;

import javafx.application.Platform;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Polygon;
import org.springframework.stereotype.Component;
import ru.arvoglade.Engine.window.graphiscAnchorPane.graphicsCanvas.GraphicsCanvas;

import java.util.List;

@Component("main")
public class DrawerFx implements Drawer {

    private final Canvas canvas;
    private final GraphicsContext gc;

    public DrawerFx(GraphicsCanvas canvas, GraphicsContext graphicsContext) {
        this.canvas = canvas;
        this.gc = graphicsContext;
    }

    public void draw(List<Polygon> polygons) {
        Platform.runLater(() -> {
            gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());

            gc.setFill(Paint.valueOf("202020"));
            gc.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());

            drawPolygonsInGc(polygons);
        });
    }

    private void drawPolygonsInGc(List<Polygon> polygons) {
        polygons.forEach(this::drawOnePolygon);
    }

    private void drawOnePolygon(Polygon polygon) {
        int pointsCount = polygon.getPoints().size() / 2;
        double[] xArr = new double[pointsCount];
        double[] yArr = new double[pointsCount];
        fillArrays(polygon, xArr, yArr);
        gc.setFill(polygon.getFill());
        gc.fillPolygon(xArr, yArr, pointsCount);
    }

    private void fillArrays(Polygon polygon, double[] xCoordinates, double[] yCoordinates) {
        List<Double> points = polygon.getPoints();
        for (int i = 1; i < points.size(); i += 2) {
            xCoordinates[(i - 1) / 2] = points.get(i - 1);
            yCoordinates[(i - 1) / 2] = points.get(i);
        }
    }
}
