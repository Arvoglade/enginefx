package ru.arvoglade.Engine.process.drawer;

import javafx.application.Platform;
import javafx.scene.shape.Polygon;

import java.util.List;

public interface Drawer {

    void draw(List<Polygon> polygons);

}
