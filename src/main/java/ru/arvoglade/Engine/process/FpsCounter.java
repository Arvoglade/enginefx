package ru.arvoglade.Engine.process;

import javafx.application.Platform;
import org.springframework.stereotype.Component;
import ru.arvoglade.Engine.window.userUI.uiVBox.dataVBox.FPSLabel;

import java.util.concurrent.atomic.AtomicInteger;

@Component
public class FpsCounter {

    private final FPSLabel fpsLabel;
    volatile AtomicInteger counter = new AtomicInteger(0);

    public FpsCounter(FPSLabel fpsLabel) {
        this.fpsLabel = fpsLabel;
    }


    public void start() {

        Thread fpsViewerThread = new Thread(() -> {
            while (true) {

                Platform.runLater(() -> {
                    fpsLabel.setText(" fps: " + counter.get());
                    counter.set(0);
                });

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        fpsViewerThread.start();
    }

    public void increment() {
        counter.incrementAndGet();
    }
}
