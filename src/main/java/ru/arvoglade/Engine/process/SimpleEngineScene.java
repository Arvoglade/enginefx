package ru.arvoglade.Engine.process;

import lombok.Data;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.arvoglade.Engine.model.Object3D;

import java.util.HashMap;
import java.util.Map;

@Data
@Component("default")
@Scope("prototype")
@Deprecated
public class SimpleEngineScene {

    private Camera camera;
    private Map<String, Object3D> objectsMap;

    public SimpleEngineScene(Camera camera) {
        this.camera = camera;
        this.objectsMap = new HashMap<>();
    }
}
