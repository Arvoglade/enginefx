package ru.arvoglade.Engine.process;

import javafx.geometry.Point3D;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.arvoglade.Engine.model.Object3D;
import ru.arvoglade.Engine.model.Orientation;

@Data
@Component
public class Camera extends Object3D {

    @Value("${camera.viewField}")
    private double viewField = 1.5; // radians

    @Value("${camera.viewDistance}")
    private double viewDistance = 0;

    double speedValue = 10;
    protected Point3D intendedSpeedVector = Point3D.ZERO;

    private double rollSpeedMoment = 0;
    private double rollSpeedValue = 0.02;

    /**
     * Moves camera according to its speed and according to its orientation
     */
    @Override
    public void move() {
        location = getCoordinatesOfPointInOldCoordinateSystem(speedVector, location, orientation);
        rotateLocal(new Orientation(0, 0, rollSpeedMoment));
    }

    public void moveForward(boolean isActive) {
        if (isActive)
            intendedSpeedVector = new Point3D(speedValue, intendedSpeedVector.getY(), intendedSpeedVector.getZ());
        else
            intendedSpeedVector = new Point3D(0, intendedSpeedVector.getY(), intendedSpeedVector.getZ());
        speedVector = getRealSpeedVector(intendedSpeedVector);
    }


    public void moveBackward(boolean isActive) {
        if (isActive)
            intendedSpeedVector = new Point3D(-speedValue, intendedSpeedVector.getY(), intendedSpeedVector.getZ());
        else
            intendedSpeedVector = new Point3D(0, intendedSpeedVector.getY(), intendedSpeedVector.getZ());
        speedVector = getRealSpeedVector(intendedSpeedVector);
    }

    public void moveLeft(boolean isActive) {
        if (isActive)
            intendedSpeedVector = new Point3D(intendedSpeedVector.getX(), speedValue, intendedSpeedVector.getZ());
        else
            intendedSpeedVector = new Point3D(intendedSpeedVector.getX(), 0, intendedSpeedVector.getZ());
        speedVector = getRealSpeedVector(intendedSpeedVector);
    }

    public void moveRight(boolean isActive) {
        if (isActive)
            intendedSpeedVector = new Point3D(intendedSpeedVector.getX(), -speedValue, intendedSpeedVector.getZ());
        else
            intendedSpeedVector = new Point3D(intendedSpeedVector.getX(), 0, intendedSpeedVector.getZ());
        speedVector = getRealSpeedVector(intendedSpeedVector);
    }

    public void moveUp(boolean isActive) {
        if (isActive)
            intendedSpeedVector = new Point3D(intendedSpeedVector.getX(), intendedSpeedVector.getY(), speedValue);
        else
            intendedSpeedVector = new Point3D(intendedSpeedVector.getX(), intendedSpeedVector.getY(), 0);
        speedVector = getRealSpeedVector(intendedSpeedVector);
    }

    public void moveDown(boolean isActive) {
        if (isActive)
            intendedSpeedVector = new Point3D(intendedSpeedVector.getX(), intendedSpeedVector.getY(), -speedValue);
        else
            intendedSpeedVector = new Point3D(intendedSpeedVector.getX(), intendedSpeedVector.getY(), 0);
        speedVector = getRealSpeedVector(intendedSpeedVector);
    }

    private Point3D getRealSpeedVector(Point3D intendedSpeedVector) {
        int nonNullSpeedAxles = 0;
        if (intendedSpeedVector.getX() != 0) {
            nonNullSpeedAxles++;
        }
        if (intendedSpeedVector.getY() != 0) {
            nonNullSpeedAxles++;
        }
        if (intendedSpeedVector.getZ() != 0) {
            nonNullSpeedAxles++;
        }

        if (nonNullSpeedAxles == 0) {
            return intendedSpeedVector;
        }
        speedVector = intendedSpeedVector.multiply(Math.sqrt(1. / nonNullSpeedAxles));
        return speedVector;
    }


    public void rollPositive(boolean isActive) {
        if (isActive)
            rollSpeedMoment = rollSpeedValue;
        else
            rollSpeedMoment = 0;
    }

    public void rollNegative(boolean isActive) {
        if (isActive)
            rollSpeedMoment = -rollSpeedValue;
        else
            rollSpeedMoment = 0;
    }

}
