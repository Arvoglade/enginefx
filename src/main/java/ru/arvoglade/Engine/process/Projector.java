package ru.arvoglade.Engine.process;

import javafx.geometry.Point2D;
import javafx.geometry.Point3D;
import javafx.scene.shape.Polygon;
import org.springframework.stereotype.Component;
import ru.arvoglade.Engine.model.Polygon3D;
import ru.arvoglade.Engine.model.Object3D;
import ru.arvoglade.Engine.model.Orientation;
import ru.arvoglade.Engine.window.graphiscAnchorPane.graphicsCanvas.GraphicsCanvas;

import java.util.*;

import static java.lang.Math.*;

@Component
public class Projector {

    double viewField = 1;
    double screenHalfWidth = 500;
    double screenHalfHeight = 500;

    private GraphicsCanvas graphicsCanvas;

    public Projector(GraphicsCanvas graphicsCanvas) {
        this.graphicsCanvas = graphicsCanvas;
    }

    public List<Polygon> getSceneProjection(Camera camera, List<Object3D> objects) {
        viewField = camera.getViewField();

        screenHalfWidth = graphicsCanvas.getWidth() / 2;
        screenHalfHeight = graphicsCanvas.getHeight() / 2;

        List<Polygon3D> polygons3D = new ArrayList<>();
        objects.forEach(o -> polygons3D.addAll(o.getPolygons()));

        List<Polygon3D> polygons3DInCameraCoordinateSystem = calculate3D(camera, polygons3D);

        polygons3DInCameraCoordinateSystem.sort(new Comparator<Polygon3D>() {
            @Override
            public int compare(Polygon3D p1, Polygon3D p2) {
                //return Double.compare(getAverageHeightFromPolygon3D(p1), getAverageHeightFromPolygon3D(p2));
                return Double.compare(getAverageDistanceToPolygon(p2), getAverageDistanceToPolygon(p1));
            }
        });

        List<Polygon> polygons2DForScreen = calculate2D(camera, polygons3DInCameraCoordinateSystem);
        return polygons2DForScreen;
    }

    private List<Polygon3D> calculate3D(Camera camera, List<Polygon3D> polygons3D) {
        List<Polygon3D> poligonsFromCameraList = new ArrayList<>();

        Point3D cameraLocation = camera.getLocation();
        Orientation cameraOrientation = camera.getOrientation();


        for (Polygon3D polygon : polygons3D) {
            Point3D polygonLocation = polygon.getParentObject().getLocation();
            Orientation polygonOrientation = polygon.getParentObject().getOrientation();

            List<Point3D> calculatedPoints = new ArrayList<>();
            for (Point3D pointLocal : polygon.getPoints()) {
                pointLocal = pointLocal.subtract(polygon.getParentObject().getCenter());

                Point3D pointGlobal = Object3D.getCoordinatesOfPointInOldCoordinateSystem(pointLocal,
                        polygonLocation, polygonOrientation);

                Point3D pointFromCamera = Object3D.getCoordinatesOfPointInAnotherCoordinateSystem(pointGlobal,
                        cameraLocation, cameraOrientation);

                calculatedPoints.add(pointFromCamera);
            }

            Polygon3D polygonFromCamera = new Polygon3D(calculatedPoints);
            if (!filterByViewAngle(camera, polygonFromCamera)) {
                continue;
            }
            polygonFromCamera.setPaint(polygon.getPaint());
            poligonsFromCameraList.add(polygonFromCamera);
        }
        return poligonsFromCameraList;
    }

    private List<Polygon> calculate2D(Camera camera, List<Polygon3D> localPolygons) {
        List<Polygon> resultPolygons = new ArrayList<>();
        for (Polygon3D polygon : localPolygons) {

            polygon = cutPolygonPartIfBehindCamera(polygon);

            Polygon polygon2D = new Polygon();
            polygon2D.setFill(polygon.getPaint());
            for (Point3D pointLocal : polygon.getPoints()) {
                // then find angles and transform them into x and y coordinates of the screen


                // NEW
                double x = pointLocal.getX();
                double y = pointLocal.getY();
                double z = pointLocal.getZ();

                double rAngle = atan2(Math.sqrt(y * y + z * z), x);
                double r = rAngle / PI * screenHalfWidth * 2 * (PI / viewField);
                double fi = atan2(z, y);

                double X = screenHalfWidth - r * cos(fi);
                double Y = screenHalfHeight - r * sin(fi);

                polygon2D.getPoints().add(X);
                polygon2D.getPoints().add(Y);
            }
            if (isFrontSide(polygon2D)) {
                resultPolygons.add(polygon2D);
            }

        }
        return resultPolygons;

    }

    private Polygon3D cutPolygonPartIfBehindCamera(Polygon3D polygon) {
        boolean needToBeCut = false;
        int start = -1;
        for (int i = 0; i < polygon.getPoints().size(); i++) {
            if (polygon.getPoints().get(i).getX() < 0) {
                needToBeCut = true;
            } else if (start == -1) {
                start = i;
            }
        }
        if (!needToBeCut)
            return polygon;
        if (start == -1) {
            return new Polygon3D();
        }
        else {
            //
            List<Point3D> pointsNew = new ArrayList<>();
            int size = polygon.getPoints().size();
            for (int j = 0; j < size; j++) {
                int i = (start + j < size) ? (start + j) : (start + j - size);

                Point3D p1 = polygon.getPoints().get(i);
                Point3D p2 = polygon.getPoints().get((i + 1 < size) ? (i + 1) : (i + 1 - size));

                if (p1.getX() >= 0) {
                    pointsNew.add(p1);
                    if (p2.getX() < 0) {
                        pointsNew.add(computeZeroPoint(p1, p2));
                    }
                } else {
                    if (p2.getX() >= 0) {
                        pointsNew.add(computeZeroPoint(p1, p2));
                    }
                }
            }
            polygon.setPoints(pointsNew);
            return polygon;
        }
    }

    private Point3D computeZeroPoint(Point3D p1, Point3D p2) {
        double a, b, c1, c2;
        double dx = (p2.getX() - p1.getX());
        if (dx != 0) {
            a = (p2.getY() - p1.getY()) / dx;
            b = (p2.getZ() - p1.getZ()) / dx;
        } else {
            a = Double.POSITIVE_INFINITY;
            b = Double.POSITIVE_INFINITY;
        }
        c1 = p1.getY() - a * p1.getX();
        c2 = p1.getZ() - b * p1.getX();

        double y = c1;
        double z = c2;
        return new Point3D(0, y, z);
    }

    public static double getAverageHeightFromPolygon3D(Polygon3D polygon3D) {
        int pointsCount = polygon3D.getPoints().size();
        double heightSum = polygon3D.getPoints().stream()
                .mapToDouble(Point3D::getZ)
                .sum();
        return heightSum / pointsCount;
    }

    public static double getAverageDistanceToPolygon(Polygon3D polygon3D) {
        double sum = polygon3D.getPoints().stream()
                .mapToDouble((p) -> p.distance(Point3D.ZERO))
                .sum();
        return sum / polygon3D.getPoints().size();
    }

    private boolean filterByViewAngle(Camera camera, Polygon3D polygon) {

        for (Point3D point : polygon.getPoints()) {
            Point3D cameraDirectionVector = new Point3D(1, 0, 0);
            if (point.angle(cameraDirectionVector) < 90) {
                return true;
            }
        }
        return false;
    }

    public boolean isFrontSide(Polygon polygon) {

        List<Point2D> points = new ArrayList<>();

        for (int i = 1; i <= polygon.getPoints().size(); i += 2) {
            points.add(new Point2D(polygon.getPoints().get(i - 1), polygon.getPoints().get(i)));
        }
        if (points.size() < 3)
            return false;

        Point2D vector1 = points.get(1).subtract(points.get(0));
        Point2D vector2 = points.get(2).subtract(points.get(1));

        return vector1.crossProduct(vector2).getZ() < 0;
    }
}
