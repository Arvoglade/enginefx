package ru.arvoglade.Engine.process;

import javafx.application.Platform;
import org.springframework.stereotype.Component;
import ru.arvoglade.Engine.window.userUI.uiVBox.dataVBox.coordinates.*;

@Component
public class UIupdater {

    private final Camera camera;
    private final CoordinateX coordinateX;
    private final CoordinateY coordinateY;
    private final CoordinateZ coordinateZ;
    private final CoordinateYaw coordinateYaw;
    private final CoordinatePitch coordinatePitch;
    private final CoordinateRoll coordinateRoll;

    public UIupdater(Camera camera, CoordinateX coordinateX, CoordinateY coordinateY, CoordinateZ coordinateZ,
                     CoordinateYaw coordinateYaw, CoordinatePitch coordinatePitch, CoordinateRoll coordinateRoll) {
        this.camera = camera;
        this.coordinateX = coordinateX;
        this.coordinateY = coordinateY;
        this.coordinateZ = coordinateZ;
        this.coordinateYaw = coordinateYaw;
        this.coordinatePitch = coordinatePitch;
        this.coordinateRoll = coordinateRoll;
    }


    public void update() {
        Platform.runLater(() -> {
            coordinateX.setText(" x: " + ((int) camera.getLocation().getX()));
            coordinateY.setText(" y: " + ((int) camera.getLocation().getY()));
            coordinateZ.setText(" z: " + ((int) camera.getLocation().getZ()));
            coordinateYaw.setText(" yaw: " + ((int) (camera.getOrientation().getYaw() * 180 / Math.PI)));
            coordinatePitch.setText(" pitch: " + ((int) (camera.getOrientation().getPitch() * 180 / Math.PI)));
            coordinateRoll.setText(" roll: " + ((int) (camera.getOrientation().getRoll() * 180 / Math.PI)));
        });

    }





}
