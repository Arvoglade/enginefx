package ru.arvoglade.Engine.process;

import javafx.stage.Screen;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import ru.arvoglade.Engine.engineCore.EngineFacade;
import ru.arvoglade.Engine.engineCore.EngineSceneRunner;
import ru.arvoglade.Engine.window.graphiscAnchorPane.graphicsCanvas.GraphicsCanvas;
import ru.arvoglade.Engine.window.userUI.cursor.Cursor;

@Component
public class TemporalEngineSceneStarter implements CommandLineRunner {

    //private EngineSceneRunnerOld engineSceneRunnerOld;

    //private EngineSceneRunner engineSceneRunner;

    private final EngineFacade engineFacade;

    private GraphicsCanvas canvas;

    private Cursor cursor;

    @Value("${scene}")
    private String sceneName;

    public TemporalEngineSceneStarter(
            GraphicsCanvas canvas,
            Cursor cursor,
            EngineSceneRunner engineSceneRunner, EngineFacade engineFacade) {
        //this.engineSceneRunner = engineSceneRunner;
        this.canvas = canvas;
        this.cursor = cursor;
        this.engineFacade = engineFacade;
    }

    @Override
    public void run(String... args) throws Exception {
        canvas.setWidth(Screen.getPrimary().getBounds().getWidth());
        canvas.setHeight(Screen.getPrimary().getBounds().getHeight());

        engineFacade.startScene(sceneName);

        cursor.drawPointer();
    }
}
