package ru.arvoglade.Engine.window.graphiscAnchorPane.graphicsCanvas;

import jakarta.annotation.PostConstruct;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;


@Component
public class GraphicsCanvas extends Canvas {

    @Value("${window.graphicsPaneSize.height}")
    private double height;
    @Value("${window.graphicsPaneSize.width}")
    private double width;

    public GraphicsCanvas() {

    }

    @PostConstruct
    public void init() {
        this.setWidth(width);
        this.setHeight(height);
    }

    @Bean
    public GraphicsContext getGraphicsContext(GraphicsCanvas canvas) {
        return canvas.getGraphicsContext2D();
    }
}

