package ru.arvoglade.Engine.window.graphiscAnchorPane;

import javafx.scene.layout.*;
import org.springframework.stereotype.Component;
import ru.arvoglade.Engine.window.graphiscAnchorPane.graphicsCanvas.GraphicsCanvas;

@Component
public class GraphicsAnchorPane extends AnchorPane {

    public GraphicsAnchorPane(GraphicsCanvas graphicsCanvas) {
        this.getChildren().add(graphicsCanvas);
    }
}
