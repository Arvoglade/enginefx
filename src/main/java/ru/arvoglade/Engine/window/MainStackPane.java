package ru.arvoglade.Engine.window;

import javafx.scene.layout.StackPane;
import org.springframework.stereotype.Component;
import ru.arvoglade.Engine.window.graphiscAnchorPane.GraphicsAnchorPane;
import ru.arvoglade.Engine.window.userUI.UserUI;

@Component
public class MainStackPane extends StackPane {

    public MainStackPane(GraphicsAnchorPane graphicsAnchorPane,
                         UserUI userUI) {
        this.getChildren().add(graphicsAnchorPane);
        this.getChildren().add(userUI);
    }
}
