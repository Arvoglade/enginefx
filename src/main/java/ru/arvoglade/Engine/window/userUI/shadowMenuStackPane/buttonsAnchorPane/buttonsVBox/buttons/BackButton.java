package ru.arvoglade.Engine.window.userUI.shadowMenuStackPane.buttonsAnchorPane.buttonsVBox.buttons;


import javafx.scene.control.Button;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class BackButton extends Button implements ApplicationContextAware {

    private ApplicationContext context;

    {
        this.setText("Back");
        /*this.setMaxSize(50, 25);
        this.setMinSize(50, 25);*/

        this.setOnAction((event) -> {

        });
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }
}
