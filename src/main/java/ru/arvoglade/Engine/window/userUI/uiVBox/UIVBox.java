package ru.arvoglade.Engine.window.userUI.uiVBox;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import org.springframework.stereotype.Component;
import ru.arvoglade.Engine.window.userUI.UserUIChildrenI;
import ru.arvoglade.Engine.window.userUI.uiVBox.dataVBox.DataVBox;

@Component
public class UIVBox extends VBox implements UserUIChildrenI {

    private Canvas canvas;

    public UIVBox(DataVBox dataVBox) {

        canvas = new Canvas();
        Pane pane = new AnchorPane(canvas, dataVBox);
        AnchorPane.setLeftAnchor(canvas, 0.);
        this.getChildren().addAll(pane);
        this.setVisible(false);
    }

    public void showOrHide() {
        if (isVisible()) {
            this.setVisible(false);
        } else {
            double width = 60;
            double height = 120;
            canvas.setWidth(width);
            canvas.setHeight(height);
            GraphicsContext gc = canvas.getGraphicsContext2D();
            gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
            gc.setFill(Paint.valueOf("ffffff99"));
            gc.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
            this.setVisible(true);
        }
    }

    @Override
    public int getOrder() {
        return 5;
    }
}
