package ru.arvoglade.Engine.window.userUI.uiVBox.dataVBox.coordinates;

import javafx.scene.control.Label;
import org.springframework.stereotype.Component;

@Component
public class CoordinateX extends Label implements DataCoordinateLabel {
    @Override
    public int getOrder() {
        return 0;
    }
}
