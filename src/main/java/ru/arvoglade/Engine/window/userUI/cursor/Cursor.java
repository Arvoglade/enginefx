package ru.arvoglade.Engine.window.userUI.cursor;

import javafx.scene.canvas.Canvas;
import javafx.scene.paint.Paint;
import org.springframework.stereotype.Component;
import ru.arvoglade.Engine.window.graphiscAnchorPane.graphicsCanvas.GraphicsCanvas;
import ru.arvoglade.Engine.window.userUI.UserUIChildrenI;

@Component
public class Cursor extends Canvas implements UserUIChildrenI {


    double screenHeight = 1080;
    double screenWidth = 1920;

    public Cursor(GraphicsCanvas graphicsCanvas) {
        screenWidth = graphicsCanvas.getWidth();
        screenHeight = graphicsCanvas.getHeight();
        this.setWidth(screenWidth);
        this.setHeight(screenHeight);
    }

    public void drawPointer() {
        this.getGraphicsContext2D().setStroke(Paint.valueOf("ffffff"));
        double heightCenter = screenHeight / 2;
        double widthCenter = screenWidth / 2;
        this.getGraphicsContext2D().strokeLine(widthCenter - 10, heightCenter, widthCenter + 10, heightCenter);
        this.getGraphicsContext2D().strokeLine(widthCenter, heightCenter - 10, widthCenter, heightCenter + 10);
    }

    @Override
    public int getOrder() {
        return 1;
    }
}
