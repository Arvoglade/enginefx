package ru.arvoglade.Engine.window.userUI;

import javafx.scene.Node;
import javafx.scene.layout.StackPane;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.List;

@Component
public class UserUI extends StackPane {

    public UserUI(List<UserUIChildrenI> children) {
        List<Node> childNodes = children.stream()
                .sorted(Comparator.comparingInt(UserUIChildrenI::getOrder))
                .map(Node.class::cast).toList();
        this.getChildren().addAll(childNodes);
    }

}
