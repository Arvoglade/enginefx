package ru.arvoglade.Engine.window.userUI.shadowMenuStackPane.buttonsAnchorPane.buttonsVBox;

import javafx.scene.layout.VBox;
import org.springframework.stereotype.Component;
import ru.arvoglade.Engine.window.userUI.shadowMenuStackPane.buttonsAnchorPane.buttonsVBox.buttons.ExitButton;

@Component
public class ButtonsVBox extends VBox {

    public ButtonsVBox(ExitButton exitButton) {
        this.getChildren().add(exitButton);
    }
}
