package ru.arvoglade.Engine.window.userUI.uiVBox.dataVBox;

import javafx.scene.Node;
import javafx.scene.layout.VBox;
import org.springframework.stereotype.Component;
import ru.arvoglade.Engine.window.userUI.uiVBox.dataVBox.coordinates.DataCoordinateLabel;

import java.util.Comparator;
import java.util.List;

@Component
public class DataVBox extends VBox {

    public DataVBox(List<DataCoordinateLabel> dataCoordinatesLabel, FPSLabel fpsLabel) {
        List<Node> coordinatesNodes = dataCoordinatesLabel.stream()
                        .sorted(Comparator.comparingInt(DataCoordinateLabel::getOrder))
                        .map(Node.class::cast)
                        .toList();

        this.getChildren().addAll(coordinatesNodes);
        this.getChildren().add(fpsLabel);
    }
}
