package ru.arvoglade.Engine.window.userUI.shadowMenuStackPane.buttonsAnchorPane.buttonsVBox.buttons;

import javafx.scene.control.Button;
import javafx.scene.text.Font;
import org.springframework.stereotype.Component;

@Component
public class ExitButton extends Button {

    {
        this.setPrefSize(100, 30);
        this.setText("Exit");
        this.setFont(Font.font(25));
        this.setOnAction(e -> {
            System.exit(0);
        });
    }

}
