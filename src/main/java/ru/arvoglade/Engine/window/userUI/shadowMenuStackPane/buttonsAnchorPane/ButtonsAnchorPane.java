package ru.arvoglade.Engine.window.userUI.shadowMenuStackPane.buttonsAnchorPane;

import javafx.scene.layout.AnchorPane;
import org.springframework.stereotype.Component;
import ru.arvoglade.Engine.window.userUI.shadowMenuStackPane.buttonsAnchorPane.buttonsVBox.ButtonsVBox;

@Component
public class ButtonsAnchorPane extends AnchorPane {

    public ButtonsAnchorPane(ButtonsVBox buttonsVBox) {

        this.getChildren().add(buttonsVBox);
    }


}
