package ru.arvoglade.Engine.window;

import javafx.scene.Scene;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SceneConfiguration {

    @Bean
    public Scene getScene(MainStackPane stackPane) {
        Scene scene = new Scene(stackPane);
        return scene;
    }
}
