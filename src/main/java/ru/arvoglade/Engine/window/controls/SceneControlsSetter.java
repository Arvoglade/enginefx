package ru.arvoglade.Engine.window.controls;

import jakarta.annotation.PostConstruct;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.robot.Robot;
import org.springframework.stereotype.Component;
import ru.arvoglade.Engine.model.Orientation;
import ru.arvoglade.Engine.process.Camera;
import ru.arvoglade.Engine.window.userUI.shadowMenuStackPane.ShadowMenuStackPane;
import ru.arvoglade.Engine.window.userUI.uiVBox.UIVBox;

import java.util.HashMap;
import java.util.Map;

@Component
public class SceneControlsSetter {

    private final Camera camera;
    private final UIVBox uivBox;
    private Map<String, Runnable> actionsOnKeys = getActionsOnKeys(true);
    private Map<String, Runnable> cancelActionsOnKeys = getActionsOnKeys(false);
    private Scene scene;
    private ShadowMenuStackPane shadowMenuStackPane;
    private Robot robot;
    private double cursorDefaultPositionX;
    private double cursorDefaultPositionY;

    private static final String FORWARD_KEY = "W";
    private static final String BACKWARD_KEY = "S";
    private static final String LEFT_KEY = "A";
    private static final String RIGHT_KEY = "D";
    private static final String UP_KEY = "SPACE";
    private static final String DOWN_KEY = "C";
    private static final String ROLL_LEFT_KEY = "Q";
    private static final String ROLL_RIGHT_KEY = "E";
    private static final String ESCAPE_KEY = "Esc";
    private static final String SHOW_UI_KEY = "F3";
    private boolean isCursorLockedToCamera = true;

    public SceneControlsSetter(Camera camera, Scene scene, ShadowMenuStackPane shadowMenuStackPane, UIVBox dataVBox) {
        this.camera = camera;
        this.scene = scene;
        this.shadowMenuStackPane = shadowMenuStackPane;
        this.uivBox = dataVBox;
    }

    @PostConstruct
    public void init() {
        scene.setOnMouseMoved(getMouseMovedEvent());
        scene.setOnKeyPressed(getKeyPressedEvent());
        scene.setOnKeyReleased(getKeyReleasedEvent());

        this.robot = new Robot();
        this.cursorDefaultPositionX = 500.;
        this.cursorDefaultPositionY = 500.;
        robot.mouseMove(cursorDefaultPositionX, cursorDefaultPositionY);
        scene.setCursor(Cursor.NONE);
    }

    private EventHandler<? super MouseEvent> getMouseMovedEvent() {
        return (EventHandler<MouseEvent>) mouseEvent -> {
            if (isCursorLockedToCamera) {
                double speed = -0.005;
                double x = mouseEvent.getX() - cursorDefaultPositionX;
                double y = mouseEvent.getY() - cursorDefaultPositionY;
                double yaw = speed * x;
                double pitch = speed * y;
                camera.rotateLocal(new Orientation(yaw, pitch, 0));
                robot.mouseMove(cursorDefaultPositionX, cursorDefaultPositionY);
            }
        };
    }

    public EventHandler<? super KeyEvent> getKeyPressedEvent() {
        return (EventHandler<KeyEvent>) keyEvent -> {
            String keyCode = keyEvent.getCode().toString();
            if (actionsOnKeys.containsKey(keyCode)) {
                actionsOnKeys.get(keyCode).run();
            }

            else if (keyEvent.getCode().getName().equals(ESCAPE_KEY)) {
                shadowMenuStackPane.showOrHide(scene.getWidth(), scene.getHeight());
                if (isCursorLockedToCamera) {
                    isCursorLockedToCamera = false;
                    scene.setCursor(Cursor.DEFAULT);
                } else {
                    scene.setCursor(Cursor.NONE);
                    isCursorLockedToCamera = true;
                    robot.mouseMove(cursorDefaultPositionX, cursorDefaultPositionY);
                }
            }

            else if (keyEvent.getCode().getName().equals(SHOW_UI_KEY)) {
                uivBox.showOrHide();
            }
        };
    }

    public EventHandler<? super KeyEvent> getKeyReleasedEvent() {
        return (EventHandler<KeyEvent>) keyEvent -> {
            String keyCode = keyEvent.getCode().toString();
            if (cancelActionsOnKeys.containsKey(keyCode)) {
                cancelActionsOnKeys.get(keyCode).run();
            }
        };
    }

    public Map<String, Runnable> getActionsOnKeys(boolean isActive) {

        Map<String, Runnable> map = new HashMap<>();
        map.put(FORWARD_KEY, () -> {
           camera.moveForward(isActive);
        });
        map.put(BACKWARD_KEY, () -> {
            camera.moveBackward(isActive);
        });
        map.put(LEFT_KEY, () -> {
            camera.moveLeft(isActive);
        });
        map.put(RIGHT_KEY, () -> {
            camera.moveRight(isActive);
        });
        map.put(DOWN_KEY, () -> {
            camera.moveDown(isActive);
        });
        map.put(UP_KEY, () -> {
            camera.moveUp(isActive);
        });

        map.put(ROLL_LEFT_KEY, () -> {
            camera.rollPositive(isActive);
        });
        map.put(ROLL_RIGHT_KEY, () -> {
            camera.rollNegative(isActive);
        });

        // ADDITIONAL "LOOK AROUND" KEYS FOR TEST
        double rotationSpeed = 0.02;
        map.put("NUMPAD4", () -> {
            camera.rotateLocal(new Orientation(rotationSpeed, 0, 0));
        });
        map.put("NUMPAD6", () -> {
            camera.rotateLocal(new Orientation(-rotationSpeed, 0, 0));
        });
        map.put("NUMPAD8", () -> {
            camera.rotateLocal(new Orientation(0, rotationSpeed, 0));
        });
        map.put("NUMPAD2", () -> {
            camera.rotateLocal(new Orientation(0, -rotationSpeed, 0));
        });

        return map;
    }

}
