package ru.arvoglade.Engine.launch;

import javafx.application.Application;
import javafx.stage.Stage;
import ru.arvoglade.Engine.window.StageHolder;

public class EngineApplication extends Application {

    private static String[] args;

    @Override
    public void start(Stage primaryStage) throws Exception {
        StageHolder.setStage(primaryStage);
        SpringRunner.startSpring(args);
        primaryStage.show();
    }

    public static void run(String... args) {
        EngineApplication.args = args;
        launch();
    }

}
