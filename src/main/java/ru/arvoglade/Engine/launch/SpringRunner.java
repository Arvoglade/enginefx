package ru.arvoglade.Engine.launch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "ru.arvoglade.Engine")
public class SpringRunner {

	public static void startSpring(String[] args) {
		System.out.println("Before Spring");
		SpringApplication.run(SpringRunner.class, args);
		System.out.println("After Spring");
	}

}
