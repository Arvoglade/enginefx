package ru.arvoglade.Engine.model;

import lombok.Data;

@Data
public class Orientation {
    private double yaw;
    private double pitch;
    private double roll;

    public Orientation() {
        this.yaw = 0;
        this.pitch = 0;
        this.roll = 0;
    }
    public Orientation(double yaw, double pitch, double roll) {
        this.yaw = yaw;
        this.pitch = pitch;
        this.roll = roll;
    }

    public Orientation negative() {
        return new Orientation(-yaw, -pitch, -roll);
    }

    @Override
    public String toString() {
        return "Orientation [" +
                "yaw = " + yaw * 180 / Math.PI +
                ", pitch = " + pitch * 180 / Math.PI +
                ", roll = " + roll * 180 / Math.PI +
                ']';
    }
}
