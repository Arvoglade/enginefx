package ru.arvoglade.Engine.model;

import javafx.geometry.Point3D;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.lang.Math.*;

@Data
public class  Object3D {

    protected List<Polygon3D> polygons;

    protected Point3D center;

    protected Point3D location;

    protected Point3D speedVector;

    protected Orientation orientation;

    protected Orientation rotationAxleOrientation;

    protected double rotationSpeed;

    protected String name;

    protected String id;

    protected Orientation rotationComponent;

    protected double mass;

    public Object3D(List<Polygon3D> polygons, Point3D location, Point3D center, Orientation orientation) {
        this.polygons = polygons;
        this.center = center;
        this.location = location;
        this.orientation = orientation;
        this.name = UUID.randomUUID().toString();
        this.id = UUID.randomUUID().toString();

        this.speedVector = Point3D.ZERO;
        this.rotationAxleOrientation = new Orientation();
        this.rotationComponent = new Orientation();
        this.mass = 1;
        injectObjectToPolygons();
    }

    public Object3D(List<Polygon3D> polygons, Point3D location, Point3D center) {
        this(polygons, location, center, new Orientation(0, 0 ,0));
    }

    public Object3D(List<Polygon3D> polygons, Point3D location) {
        this(polygons, location, new Point3D(0, 0 ,0), new Orientation(0, 0 ,0));
    }

    public Object3D(List<Polygon3D> polygons) {
        this(polygons, new Point3D(0, 0 ,0), new Point3D(0, 0 ,0), new Orientation(0, 0 ,0));
    }

    public Object3D() {
        this(new ArrayList<>(), new Point3D(0, 0 ,0), new Point3D(0, 0 ,0), new Orientation(0, 0 ,0));
    }


    /**
     * Changes global location of object
     * @param newLocationVector - new location in global coordinates
     */
    public void relocate(Point3D newLocationVector) {
        // New
        location = newLocationVector;
    }

    /**
     * Moves object by vector
     * @param moveVector - movement vector relative to object's orientation
     */
    public void move(Point3D moveVector) {
        location = location.add(moveVector);
    }

    /**
     * Moves object according to its speed. This method is intended to be used by physics
     */
    public void move() {
        location = location.add(speedVector);
    }


    /**
     * Rotates object
     * @param rotationComponent - angle of rotation in radians. Affects on rotation speed
     * @param rotationAxleOrientation - set axle for rotation
     */
    public void rotate(Orientation rotationComponent, Orientation rotationAxleOrientation) {

        // Object's orientation
        Point3D frontPoint = new Point3D(1, 0, 0);
        Point3D topPoint = new Point3D(0, 0, 1);

        if (!rotationAxleOrientation.equals(orientation)) {
            // To Global orientation
            frontPoint = getCoordinatesOfPointInOldCoordinateSystem(frontPoint, Point3D.ZERO, orientation);
            topPoint = getCoordinatesOfPointInOldCoordinateSystem(topPoint, Point3D.ZERO, orientation);

            // To rotation orientation
            frontPoint = getCoordinatesOfPointInAnotherCoordinateSystem(frontPoint, Point3D.ZERO, rotationAxleOrientation);
            topPoint = getCoordinatesOfPointInAnotherCoordinateSystem(topPoint, Point3D.ZERO, rotationAxleOrientation);
        }

        // Rotation
        frontPoint = rotatePoint(frontPoint, rotationComponent);
        topPoint = rotatePoint(topPoint, rotationComponent);

        // Back ro global orientation
        frontPoint = getCoordinatesOfPointInOldCoordinateSystem(frontPoint, Point3D.ZERO, rotationAxleOrientation);
        topPoint = getCoordinatesOfPointInOldCoordinateSystem(topPoint, Point3D.ZERO, rotationAxleOrientation);

        // Compute new orientation
        double yaw = atan2(frontPoint.getY(), frontPoint.getX());
        double pitch = asin(frontPoint.getZ());
        topPoint = Object3D.rotatePoint(topPoint, new Orientation(-yaw, -pitch, 0));
        double roll = atan2(topPoint.getY(), topPoint.getZ());

        orientation = new Orientation(yaw, pitch, roll);
    }

    /**
     * Rotates object according to its rotation speed and rotationAxle.
     * This method is intended to be used by physics
     */
    public void rotate() {
        rotate(rotationComponent, rotationAxleOrientation);
    }

    /**
     * Rotates object in its own orientation
     * @param rotation - angle of rotation in radians. Affects on rotation speed
     */
    public void rotateLocal(Orientation rotation) {
        rotate(rotation, orientation);
    }

    // basic object behavior
    public void processObject() {
        move();
        rotate();
    }

    public void setPolygons(List<Polygon3D> polygons) {
        this.polygons = polygons;
        injectObjectToPolygons();
    }

    private void injectObjectToPolygons() {
        this.polygons.forEach(p -> p.setParentObject(this));
    }

    @Override
    public String toString() {
        return "Object3D{}";
    }

    /**
     * Represents the same point in another coordinate system
     * @param initialPoint - point in old coordinate system
     * @param anotherSystemLocation - zero point of another coordinate system in old coordinate system
     * @param anotherSystemOrientation - orientation of another coordinate system in old coordinate system
     * @return point in another coordinate system
     */
    public static Point3D getCoordinatesOfPointInAnotherCoordinateSystem(
            Point3D initialPoint,
            Point3D anotherSystemLocation,
            Orientation anotherSystemOrientation
    ) {

        Point3D point;
        point = initialPoint.subtract(anotherSystemLocation);
        point = rotatePoint(point, anotherSystemOrientation.negative());
        return point;
    }

    /**
     * Finds coordinates of point in old coordinate system
     * Backward operation to getCoordinatesOfPointInAnotherCoordinateSystem.
     * If pass some point to getCoordinatesOfPointInAnotherCoordinateSystem,
     * and then to this method, return value will be equal to initial point;
     * @param initialPoint - point in another coordinate system
     * @param anotherSystemLocation - zero point of another coordinate system in old coordinate system
     * @param anotherSystemOrientation - orientation of another coordinate system in old coordinate system
     * @return - point in old coordinate system
     */
    public static Point3D getCoordinatesOfPointInOldCoordinateSystem(
            Point3D initialPoint,
            Point3D anotherSystemLocation,
            Orientation anotherSystemOrientation
    ) {
        Point3D point;
        point = rotatePoint2(initialPoint, anotherSystemOrientation);
        point = point.add(anotherSystemLocation);
        return point;
    }

    public static Point3D rotatePoint(Point3D p, Orientation orientation) {
        double x = p.getX();
        double y = p.getY();
        double z = p.getZ();

        double yaw = orientation.getYaw();
        double pitch = orientation.getPitch();
        double roll = orientation.getRoll();

        // z axle (yaw)
        double x1 = x * cos(yaw) - y * sin(yaw);
        double y1 = y * cos(yaw) + x * sin(yaw);
        double z1 = z;

        // y axle (pitch)
        double x2 = x1 * cos(pitch) - z1 * sin(pitch);
        double y2 = y1;
        double z2 = z1 * cos(pitch) + x1 * sin(pitch);

        // x axle (roll)
        double x3 = x2;
        double y3 = y2 * cos(roll) + z2 * sin(roll);
        double z3 = z2 * cos(roll) - y2 * sin(roll);

        return new Point3D(x3, y3, z3);
    }

    public static Point3D rotatePoint2(Point3D p, Orientation orientation) {
        double x = p.getX();
        double y = p.getY();
        double z = p.getZ();

        double yaw = orientation.getYaw();
        double pitch = orientation.getPitch();
        double roll = orientation.getRoll();

        // x axle (roll)
        double x1 = x;
        double y1 = y * cos(roll) + z * sin(roll);
        double z1 = z * cos(roll) - y * sin(roll);

        // y axle (pitch)
        double x2 = x1 * cos(pitch) - z1 * sin(pitch);
        double y2 = y1;
        double z2 = z1 * cos(pitch) + x1 * sin(pitch);

        // z axle (yaw)
        double x3 = x2 * cos(yaw) - y2 * sin(yaw);
        double y3 = y2 * cos(yaw) + x2 * sin(yaw);
        double z3 = z2;

        return new Point3D(x3, y3, z3);
    }

    public String getId() {
        return id;
    }
}
