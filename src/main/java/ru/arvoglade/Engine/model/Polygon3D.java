package ru.arvoglade.Engine.model;

import javafx.geometry.Point3D;
import javafx.scene.paint.Paint;
import lombok.Data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Data
public class Polygon3D {
    private List<Point3D> points = new ArrayList<>();
    private Paint paint;
    private Object3D parentObject;

    public Polygon3D(List<Point3D> points) {
        this.points = points;
        paint = Paint.valueOf("505050");
    }

    public Polygon3D(Point3D... points) {
        this(Arrays.asList(points));
    }


}
