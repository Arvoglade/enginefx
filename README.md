# Description
3D engine project to understand how engines work.
Based on JavaFX with Spring Boot.

JavaFX used to manage window application, drawing 2D shapes on the screen and controls.
Other code is to calculate 2D coordinates on screen from coordinates at 3D space.

Camera allows free rotation: no restrictions when looking at ceil or floor,
so it's possible to turn over the picture.

# Controls
    wasd - as usual
    c - down
    space - up
    q - roll left
    e - roll right

# Build and launch
## Automatically
After downloading the project execute ```Build.bat``` file. 
After console window shows "Build success" execute ```Launch.bat``` file

## Manually
Use ```mvnw clean package``` command to compile engine
Then ``` java -jar target/Engine-0.0.1-SNAPSHOT.jar``` to launch engine